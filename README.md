<div align="center">
![images/D2.png](images/D2.png)
</div>

# Pipettin Grbl Documentation

GitBuilding documentation for the pipetting robotware project at[ https://gitlab.com/pipettin-bot/pipettin-bot](https://gitlab.com/pipettin-bot/pipettin-bot). The project is released under the terms of the [CERN-OHL-S](https://gitlab.com/pipettin-bot/pipettin-bot/-/blob/master/LICENSES.md#hardware) licence for its hardware, and under the [GNU GPLv3 licence](https://gitlab.com/pipettin-bot/pipettin-bot/-/blob/master/LICENSES.md#software) licence for the software.

The rendered documentation has been published through Gitlab Pages: [https://pipettin-bot.gitlab.io/pipettin-bot-docs/](https://pipettin-bot.gitlab.io/pipettin-bot-docs/)

![toolchanger.jpg](./images/toolchanger.jpg "toolchanger.jpg")

> Tool-changer mode on!

# About this documentation

This documentation was built using "gitbuilding", and published in our project's documentation repo using GitLab pages. It is licenced under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License (SPDX: CC-BY-SA 4.0)</a> <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>.

Find it here: https://gitlab.com/pipettin-bot/pipettin-bot-docs

Please let us know if you have any questions or difficulties while using it (or modifying it).

## GitBuilding customizations

We changed some of the GitBuilding defaults as follows:

1. Custom jinja files for the HTML header (to include Hypothes.is and highlight.js by default) and body (to add some licensing information). See `footer.html.jinja` and `full_page.html.jinja`.
2. Custom CSS stylesheet for limited width images. See `assets/logo.css`

Go through the [notes](./notes) for details.

## What is GitBuilding

GitBuilding is an OpenSource project for documenting hardware projects with minimal
effort, so you can stop writing and GitBuilding. GitBuilding is a python program that
works on Windows, Linux, and MacOS. More information on the GitBuilding project, or how
to install GitBuilding please see the [GitBuilding website](http://gitbuilding.io).

## How do I edit the documentation?

Installed gitbuilding into a virtual environment named `bot_env` (or any other suitable name):

```bash
python3 -m venv bot_venv
source bot_venv/bin/activate
pip3 install gitbuilding
```

At any later time, serve the documentation locally using:

```bash
source bot_venv/bin/activate
gitbuilding serve
```

To edit the documentation you do not need to install anything, but you will need to
install something to build the final version of the documentation (such as a website).
The documentation files can be opened in a plain text editor such as Windows Notepad,
Notepad++, gedit, VS Code, etc. GitBuilding also comes with a browser-based editor that
displays has a live display of the final HTML documentation.

If you have ever used [markdown](https://www.markdownguide.org/basic-syntax/) you will
notice that the files you are editing are markdown files. GitBuilding uses an extended
markdown syntax (that we call BuildUp). This allows you to keep track of parts in the
documentation. More detail on the documentation is available on the
[GitBuilding website](https://gitbuilding.io/syntax/). There is also additional
[syntax for configuration](https://gitbuilding.io/syntax/buildconfsyntax), and for
[part libraries](https://gitbuilding.io/syntax/builduplibrary/).
