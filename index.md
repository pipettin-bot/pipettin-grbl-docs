# Pipettin' Bot

Welcome to the documentation for the pipetting robotware project, hosted at GitLab: [https://gitlab.com/pipettin-bot/pipettin-bot](https://gitlab.com/pipettin-bot/pipettin-bot)

An online version of this documentation is available through [GitLab pages](https://pipettin-bot.gitlab.io/pipettin-bot-docs/); probably what you are reading right now.

>i **Note**:
>i Though functional prototypes have been built, this documentation is in its first stage of development.
>i Please let us know if you wish to help out :)

![toolchanger.jpg](./images/toolchanger.jpg)

> Pipetting-bot is a toolchanging cartesian CNC machine and software, developed to automate liquid handling of the elemental laboratory protocols and preparations.

## Project licensing

These docs you are currently reading were made in GitBuilding, and are licensed under a Creative Commons [CC-BY-SA license](http://creativecommons.org/licenses/by-sa/4.0/).

The project is released under the terms of the [CERN-OHL-S](https://gitlab.com/pipettin-bot/pipettin-grbl/-/blob/master/HARDWARE_LICENSE.txt) license for its hardware, and under the [GNU GPLv3 licence](https://gitlab.com/pipettin-bot/pipettin-grbl/-/blob/master/SOFTWARE_LICENSE.txt) license for the software. See also the [LICENSE](https://gitlab.com/pipettin-bot/pipettin-grbl/-/blob/master/LICENSE.md) file at the project's [main GitLab repository](https://gitlab.com/pipettin-bot/pipettin-grbl).

## Documentation

>i **Note**:
>i Under construction! As of August 2022, the most mature guide is the assembly guide. The usage guide is a WIP, and the remaining guides are not yet fully populated. Development documentation is available [at the main repo](https://gitlab.com/pipettin-bot/pipettin-grbl).

There are five main guides:

1. The [assembly guide](guides/03_assembly_guide.md){step} has instructions on how to _make_ this pipetting robot.

2. The [maintenance guide](guides/02_maintenance_guide.md){step} will help you keep setup your robot, keep it in shape, and make repairs.

3. The [user guide](guides/01_user_guide.md){step} will help you setup and use the robot.

4. The [study and modification guide](guides/04_development_guide.md){step} will help you modify or adapt any aspect of the project to your use case, by explaining the software and hardware, their design ideas, working principles, strengths and limitations.

5. The [contribution guide](guides/05_contributions_guide.md){step} will help you collaborate with the project's development. All feedback and contributions are super welcome!

![pipettin.jpg](./images/pipettin.jpg)

### Sources

* [Documentation sources](https://gitlab.com/pipettin-bot/pipettin-grbl-docs) (GitLab repo).
* [Hardware and software sources](https://gitlab.com/pipettin-bot/pipettin-grbl/) (GitLab repo).

## Leave comments _on_ the documentation

We are trying out the Hypothes.is plugin as a way to leave comments here.

Select text on any of our pages, and floating menu will appear. Click on "Annotate" to add a comment related to the selected text.

Annotations in this website can be listed using [Facet](https://jonudell.info/h/facet/). Visit [this Facet URL](https://jonudell.info/h/facet/?max=500&wildcard_uri=https%3A%2F%2Fpipettin-bot.gitlab.io%2Fpipettin-grbl-docs%2F*) to browse them.

![annotate.png](./images/annotate.png)


## Get support

For now we suggest:

* Using the GOSH forum: [https://forum.openhardware.science/c/projects/54](https://forum.openhardware.science/c/projects/54)
* Creating issues on GitLab:
    * About the hardware [here](https://gitlab.com/pipettin-bot/pipettin-grbl/-/boards/4685246).
    * About the documentation [here](https://gitlab.com/pipettin-bot/pipettin-grbl-docs/-/issues).

## About this documentation

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" src="/images/80x15.png" style="width:80px;height:auto;"/></a>

## Acknowledgements

Thanks to reGOSH for sparking my interest on CNCs and teaching me the basics during the 2019 residency in Porto Alegre. I owe a grand debt of gratitude to Renan Soares for setting a great example of passion for making _truly_ free tech.

This project is partially funded by GOSH's Collaborative Development Program, that awarded us phase 1 funding on the “New Project Track”.  We thank the Gathering for Open Science Hardware ([http://openhardware.science](https://sloan.org)) and the Alfred P. Sloan Foundation ([https://sloan.org](https://sloan.org)) for their support.

[![GOSH_forum_logo.png](images/GOSH_forum_logo.png){: .logo}](https://forum.openhardware.science/)

[![reGOSH_logo.svg](images/reGOSH_logo.svg){: .logo}](https://regosh.libres.cc/en/home-en/)

<!-- logo size fixed with: https://gitlab.com/gitbuilding/gitbuilding/-/issues/252#note_1005780090 -->

