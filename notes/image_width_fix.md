MD:

```md
![GOSH_forum_logo.png](images/GOSH_forum_logo.png){: .logo}

![reGOSH_logo.svg](images/reGOSH_logo.svg){: .logo}]
```

CSS: `assets/logo.css`

```css
.logo {
  width: 200px;
}
```

Helpful links:

* https://gitbuilding.io/usage/buildconfsyntax/#custom-css-and-favicon
* https://gitlab.com/gitbuilding/gitbuilding/-/issues/252#note_971311418
* https://www.w3schools.com/css/css3_images.asp 
