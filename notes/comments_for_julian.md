# Comments for Julian on my usage of GitBuilding
## Comment 1: Licences
¿Is the "licence" configuration option for the hardware, software, or documentation?

For now:
* I explicitly stated under which licence each type of content is released in the `index.md` page.
* I added the CC-BY-SA 4.0 html thing to the footer by using `_templates/footer.html.jinja`.
* I added the SPDX identifier for the hardware licence to `buildconf.yaml`, but the distributed software is on a different license. Is there a way to add multiple licences? Or should I use the same _hack_ as in the previous item?

## Comment 2: Initial project structure
At first I found myself staring for a few minutes at the sample `index.md` page, provided by `gitbuilding init`.

Nano and I have faced this situation several times :P

Last time, when documenting the CO2 sensor stuff, we went for project "variants" to guide the documentation's structure. Each variant had code and files embedded, so hardware and software were _not_ split into different pages. I feel this type of doc fits the typical Arduino project.

This time, with a much more complex project, I decided to structure the documentation in `index.md` like this:

* links to 5 types of documentation (usage, maintenance, assembly, modification and contribution),
* and a heading with info on getting support.

My feeling is that no documentation template will work universally. However there are some usual suspects to consider:

* overview/intro/status
* usage and user support
* hardware...
* software...
* documentation
* community, contributions, development, and roadmap.

I borrowed most of these from the GitLab default readme :P

I think the GitBuilding sample pages (like `index.md`) could have a bit more text to  _guide_ the user to find a documentation structure that will work out best for their project.

Even headings or links with no content would be helpful, to spark some ideas in the new user's mind (and help overcome the _blank documentation syndrome_?).

## Comment 3: file organization
I like to keep a tidy and sortable file structure.

For now I put the 5 files for the "main guides" in the `guides/` subdirectory.

## Comment 4: internationalization
Currently I am a bit unconfortable writing in English only, because I don't know how easy it will be to maintain documentation in multiple languages using GitBuilding. I think this is a really important point.

In a way, documentation in Spanish currently makes a bit more sense for this kind of project.

Then again, Google Translate would probably do a good enough job.

## Comment 5: editor

File name completion (and all kinds of completion) would be very welcome in the editor :)

There must be a "generic" and _free_ markdown editor with such extensions.

PS: I would love to write buildup the way I write rmarkdown documents in RStudio. The completions for file names, for bibliographical references from my Zotero library, and for syntax snippets is simply awesome. At least it's worth taking a look at how their system works, since it is meant as a publishing tool.

## Comment 6: embedding images

Using `![]()` to embed images seems to stretch them to fill the full horizontal space.

I was trying to embed the GOSH logo in PNG or SVG, but could only get unreasonably large image.

## Comment 7: part libraries spreadsheet

I feel part libraries should be _importable_ from spreadsheets (i.e. CSV format or others).

Couldnt find a way to do this in the part libraries page: https://gitbuilding.io/usage/builduplibrary/

IMO spreadsheets are a better fit for writing and sharing BOMs, and while GitBuilding can export a BOM to a CSV spreadsheet ¿can it import one?

A possible counter-argument is that a YAML database can handle "multiple suppliers" for a part more naturally than a "tabular database".
