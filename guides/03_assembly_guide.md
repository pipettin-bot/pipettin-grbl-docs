# Assembly Guide

>i **Note**:
>i Under construction! See issue [https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/49](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/49).

>i Disclaimer: the bill of materials (BOM) below has links, but they are currently broken due to an issue with GitBuilding.

{{BOM}}

<!-- 03_assembly_guide.md -->

## Required skills and resources

- Patience.
- Time:
    - About 10 days worth of afternoons (~40 hours).
    - A lot of waiting, for 3D-prints and delivery of parts.
- Usage of workshop tools.
    - Unless your supplier cuts all parts to the required lengths, you will need a cutting tool for metal (aluminium and steel), for example: angle grinder, saw, etc.
    - General skills or intuition for crafting will be useful. Some parts require alignment, for which you migh use a level tool, a carpenter's square, or custom _jigs_.
    - It is important to know how to use measuring tools (calipers, measure tape) for different purposes (alignment, cutting, etc.).
    - Superglue and a hot-glue gun can come in handy.
- Wiring electronics:
    - Wire cutting, using heatshrink, very basic soldering (for soldering headers on long wires), etc.
    - No custom PCBs are needed.
    - Overally, nothing fancy, only Arduino-level stuff (i.e. dupont wires and headers).
- Basic 3D-printing:
    - Slicing, printing, post-processing.
    - Some parts may need minor sanding or lubrication. I use graphite powder.
    - A 3rd party may print stuff for you.
- Suppliers:
    - Although we used very common parts, you will have to find suppliers and deal with them.
    - A shopping list is available at the main repo, [here](https://gitlab.com/pipettin-bot/pipettin-grbl/-/tree/master/doc/BOM).
- Manufacturing equipment or services.
    - 3D printing.
    - Laser cutting: only for the electronics case, this could be 3D-printed.
    - CNC milling: for the baseplate, this coulr be laser-cut, or drilled by a skillful hand.
    - _Note_: except for the 3D-printed parts, these services are not critical to the project, and can be ommitted or replaced. For example, the removable baseplate system can be omitted, and the electronics case can be made with cardboard and hot glue.

## CNC motion system{pagestep}

In this section we'll assemble the XYZ parts for the cartesian CNC of the pipetting robot, and attach a tool-changer interface to the main carriage.

There are two versions:

- V2 (current) motion system: [01_cnc_frame-v2.md](./assembly/01_cnc_frame-v2.md){step}
    - This one has much more vertical room for taller labware.
    - Compatible with V2 (jubilee-like) and V1 (legacy) tool-changer interfaces.
- V1 (legacy) motion system: [assembly/01_cnc_frame.md](assembly/01_cnc_frame.md){step}
    - Compatible with the V1 tool-changer interface only.

![06_XYZ_axis_final.png](./assembly/01_cnc_frame/images/06_XYZ_axis_final.png)

> Expected result (legacy system).

## Support frame and transmission{pagestep}

Follow the steps at: [assembly02_structural_frame.md](./assembly/02_structural_frame.md){step}

Briefly, throughout this stage you will:

* Assemble a support structure for the XYZ parts, using 2020 aluminium profiles
* Attach the motion system to the frame.
* Add the GT2 transmission belts to the CNC frame.

![profile_lengths.png](./assembly/02_structural_frame/images/profile_lengths.png)

> Structural frame assembly.

![assembled_frame.png](./assembly/images/assembled_frame.png)

> CNC frame with the GT2 transmission belts on, and mounted on the structural frame.

## Tools and tool-posts{pagestep}

Follow the steps at: [04_tools.md](./assembly/04_tools.md){step}

Briefly, throughout this stage you will:

* Build the two pipette tools (with their tool-changer interfaces).
* Build the tool posts.
* Attach the tip-ejection post.

![p20_tool_assembled_small.jpg](./assembly/04_tools/images/p20_tool_assembled_small.jpg)

## Workspace baseplate and platform curbs{pagestep}

>i **Note**:
>i Not available (yet).

Follow the steps at: [05_baseplate.md](./assembly/05_baseplate.md){step}

Briefly, throughout this stage you will:

* Make the baseplate
* Print the platform aligners
* Test mounting some platforms.

![baseplate_small.png](./assembly/05_baseplate/images/baseplate_small.png "baseplate_small.png")

## Electronics{pagestep}

>i **Note**:
>i Not available (yet).

Follow the steps at: [03_electronics.md](./assembly/03_electronics.md){step}

Briefly, throughout this stage you will:

* Assemble and attach the electronics case.
* Attach limit switches and wire the motors.
* Manage cables.
* Wire the remaining connections (USB / GPIO).

![enclosure_small.png](./assembly/03_electronics/images/enclosure_small.png)


## Setup{pagestep}

Congratulations! You've finised the build :) Now it's time to setup the stuff!

Briefly, throughout this stage you will:

* Set up the Rapsberry Pi.
* Setup the Arduino with CNC firmware.
* Configure the CNC firmware.
* Adjust Vref on the stepper drivers.
* Run basic tests on the software and CNC firmware.

Follow the setup guide: [01_setup.md](./maintenance/01_setup.md)

# Congrats!

You've built and setup a pipetting robot. Stay hydrated!

Now your robot needs to be told where pipettes and tubes are, how to place tips, discard them, and so on...

To have it learn all of those things, proceed to the maintenance guide: [02_maintenance_guide.md](./02_maintenance_guide.md)

If you just want to play with it _now_, you may try going over the user guide: [01_user_guide.md](./01_user_guide.md)

# List of to-do's

Due improvements for this page:

- [ ] Fix the BOM links.
- [ ] Update pictures (current ones show the old Z single-pipette carriage).
- [ ] Update STLs to include the new stepper base (with the socket for the cable carrier base).
- [ ] Make the modified cablecarrier model, with a hole for inserting cables.
- [ ] …

