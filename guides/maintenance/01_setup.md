# Setup Guide

>i **Note**: 
>i Under construction. See issue [https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/52](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/52).

Will cover:

* Software and Firmware.
* End-stop adjustments.
* GRBL/CNC configuration.
* Vref adjustment.
* ...?

## Required skills and resources

Depending on your setup, this may be the most frustrating part of the guide, specially because of the:

- _Network setup_: which depends on your LAN settings and operating system (which we do not control).
- _Firmware flashing and configuration_: we provide instructions for Arduino UNO microcontrollers, each with a CNC Shield (v3.0).

## Software setup

### OS image and Firmware{pagestep}

TO-DO

Software setup:

* Raspberry Pi:
    * Pi ISO.
    * Setup network (Wired/WiFi).
    * Set or find the Pi's IP address on your local network.
* Flash microcontroller firmware:
    * Flash GRBL: https://github.com/gnea/grbl/wiki/Compiling-Grbl
    * Flash Klipper: https://www.klipper3d.org/Installation.html#building-and-flashing-the-micro-controller

A generic from-scratch setup guide for the Raspberry Pi can be found in the internet:

- "Headless" setup guide, this is what we need.
    - raspberrypi.org: [https://www.raspberrypi.com/documentation/computers/configuration.html#setting-up-a-headless-raspberry-pi](https://www.raspberrypi.com/documentation/computers/configuration.html#setting-up-a-headless-raspberry-pi)
    - Hackster: [https://www.hackster.io/435738/how-to-setup-your-raspberry-pi-headless-8a905f](https://www.hackster.io/435738/how-to-setup-your-raspberry-pi-headless-8a905f)
    - Medium: https://medium.com/hackernoon/how-to-setup-a-headless-raspberry-pi-7f2b8b00c790
    - The most relevant part of those guides is the network setup.
- Official setup guide: [https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up](https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up)
    - This one is not specific enough, but it is worth linking to.

### Software tests{pagestep}

* Test MCU connection.
* Test python modules.
* Test loading the GUI.

### Networking{pagestep}

1. Choose a connection method and follow the relevant instructions.
    * Wired / Ethernet: available on all Raspberry Pi B models.
        1. Find a suitable ethernet cable and connect the Raspberry Pi to your router.
    * Wireless / WiFi: available on RPI 4 model B, or by adding a WiFi dongle.
        1. Unplug the power supply from the Rapsberry Pi.
        1. Mount the micro SD card in you computer.
        2. Edit the `/etc/wpa_supplicant.conf` file and add your wireless network.
            - More informacion on wpa configuration can be found [here](https://www.raspberrypi.com/documentation/computers/configuration.html#adding-the-network-details-to-your-raspberry-pi).
            - We recommend also adding your mobile hotspot network to this file, in case your network blocks `nmap`, or does not support mDNS/zeroconf.
        4. Safely unmount and eject the micro SD card. Then insert it into the Raspberry Pi, and plug it to the power supply.
2. Find the Pi's IP address using a network scanning tool, such as:
    - First, try connecting to the "raspberrypi.local" address. If your network supports it, this is the easiest method.
    - Use the `nmap` tool (linux):  example: `sudo nmap -sn 192.168.0.0/24`.
        - You may add the `-p 3333` option, to scan only for the default GUI port.
        - `sudo` is required for getting the full output from `nmap`.
        - An example of the expected output is shown below.
    - Use the `Fing` app (Android).
    - Your router's admin interface.
3. Once you find the Pi's IP address (e.g. `192.168.0.13`) write it down.
    - _Note_: the Pi's IP address might expire and change. Repeat the steps above if this seems to have happened. You may also try configuring [the Pi](https://www.raspberrypi.com/documentation/computers/configuration.html#static-ip-addresses) or your router to fix it's IP (i.e. setting a "Static IP" or "DHCP Reservation", respectively), or use the Pi's hostname to connect to it (i.e. through mDNS/zeroconf to a a [raspberrypi.local](raspberrypi.local) address). Guides for this are available on the internet, specific to your internet router and operating system.
5. Test a connection to the Pi:
    - Through SSH (pw: `raspberry`):
        - Known IP: `ssh pi@192.168.0.13`
        - Using mDNS: `ssh pi@raspberrypi.local`
    - Using the browser, write the Pi's IP followed by the GUI's port (3333), and the Pipetting GUI should open.
        - Known IP: `http://192.168.0.13:3333`
        - Using mDNS: `http://raspberrypi.local:3333`

Example output from `sudo nmap  192.168.0.0/16 -p 3333`:

```
Nmap scan report for 192.168.13.52  
Host is up (0.093s latency).  
  
PORT     STATE SERVICE  
3333/tcp open  dec-notes  
MAC Address: DC:A6:32:50:03:7D (Raspberry Pi Trading)
```

## Electronics setup

### Check connections {pagestep}

TO-DO

* Logic: triple-check all electronic connections.
* Power: triple-check that nothing will catch fire, especially the PSU.

### Vref adjustment {pagestep}

The reference voltage (Vref) in a stepper driver is what limits current flow through your steppers.

The main trade-off here is the following:

- Limiting the current is a good idea! Their coils will be damaged by the excess heat produced by excess current.
    - Furthermore, the 3D-printed supports for the steppers may waken or deform under this heat.
- However, a lower current limit will cause loss of steps during high-force moves (i.e. docking tools and placing/ejecting tips).

#### How hot is _too hot_ for a stepper motor? 

A temperature around 60ºC is not terrible. If you can keep your fingers on it for at least one second, then it's not terrible.

However, since our steppers are secured to the structure using 3D-printed parts, an excess temperature will deform or even melt the plastic parts.

Ideally, you should be able to keep your hand on them for more than 5-10 seconds (_circa_ 50ºC).

See this answer for details: https://3dprinting.stackexchange.com/a/8485

#### Procedure

My notes on the subject are [here](https://wiki.frubox.org/proyectos/diy/cnc#ajuste-vref-pololu). The all3DP and old E3D guides were very helpful:
- [E3D's guide archive for the A4988 driver](https://web.archive.org/web/20210207215833/https://e3d-online.dozuki.com/Guide/VREF+adjustment+A4988/92). 
- [E3D's guide archive for the DRV8825 driver](https://web.archive.org/web/20210401111855/https://e3d-online.dozuki.com/Guide/VREF+Calibration+guide+DRV+8825/94).
- [All3DP's Vref calculator](https://all3dp.com/2/vref-calculator-tmc2209-tmc2208-a4988/).

Procedure:

1. Find the maximum current rating of your stepper motors in their specifications.
2. Calculate the reference voltage (`Vref`) for your stepper drivers and motors (e.g. A4988, DRV8225, and others).
    - See the linked guides above for details.
3. Connect the negative end of a multimeter to the controllers GND pin.
4. Connect the positive end of the multimeter to a small philips screwdriver.
6. Locate the Vref adjustment potentiometer on the drivers.
7. Connect the controller to the power suplly and turn it on.
8. Adjust the reference voltage using the screwdriver, while reading the voltage on the multimeter.
    - _Note_: if the voltage reading is 0, you may need to connect the microcontroller (in this case an Arduino) to a USB power supply.
    - The reference voltage limits the maximum current, protecting the motors from overheating. This comes at the cost of reduced torque, which may cause loss of steps in some situations (e.g. when changin tools, or placing tips).
    - If steps are lost at the maximum Vref/current setting, use larger stepper motors.
    - Also consider using a PSU with a voltage that is close to the driver's maximum voltage rating. This helps them produce _sharper_ steps.

![vref.png](./images/vref.png)

### CNC configuration{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

Configure GRBL/MCU parameters:

* Parameters (i.e. steps per revolution, speed, etc.).
* Directions.
* Parameters for homing, probing, and parking.
* Checks.
* For the CNC frame and for the pipettes.

### Electronics tests {pagestep}

<!-- Algunos tests van a estar en la guia de usage/setup. 
Dejo esto acá?
El setup guide tiene las cosas que haría después de estos pasos.-->

TO-DO

- Motors.
    - Direction.
- End-stops.
    - GRBL: Use UGS / bCNC with a laptop, or `minicom` from SSH to check.
    - Klipper: use mainsail.
- Update CNC configuration if necessary.

### CNC tests{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

* Send some commands to stuff, check that they turn in the expected directions.
    * Home the axis.
* Check end-stops, parking sensors, and probe sensors.
* Test torque with a tool-change.

### End-stop positions{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

If necessary:

* Adjust the position of the X and Y end-tops.
* Adjust the screw of the Z end-stop.
* Adjust the screw of the tool axis end-stops.
* Adjust the screws of the parking end-stops.
* Adjust the position of the tool's opto-endstops.

## Congrats!

You've setup the software and firmware for your robot. That was _tough_!

You may now:

- Continue to the object calibration guide: [02_calibration.md](./02_calibration.md)
- Return to the assembly guide: [03_assembly_guide.md](../03_assembly_guide.md)
- Return to the user guide: [01_user_guide.md](../01_user_guide.md)

# List of to-do's

Due improvements for this page:

- [ ] Write the frimware/OS burning section.
- [ ] Add screenshots for the network setup.
- [ ] Write a better network setup section.
- [ ] Write the "old PC" alternative for the Klipper version.