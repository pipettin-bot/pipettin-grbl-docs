# Calibration Guide

>i **Note**: 
>i Under construction. See issue [https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/52](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/52).

Will cover:

* Calibration of tools and objects.
* Volumetric calibration of micropipette adapters.

## Tool-changing{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.
>i 
>i Most of the content is derived from a README at the main repo. 
>i See: [doc/tool_calibration](https://gitlab.com/pipettin-bot/pipettin-grbl/-/tree/master/doc/tool_callibration)

### Intro

There are 3 important measurements for each tool:

* The `xyz` coordinates of an empty carriage when it is just in front, but clear of the **parked** tool (in red below).
* The `y_final` coordinate of a carriage when it has docked completely with a parked tool (in blue below).
* The `y_clearance` length of a tool along the Y axis, measured from the front of the carriage (in orange below).

These coordinates are explained in the following diagram:

![drawing.svg](./images/drawing.svg)

The JSON representation of the tool-change coordinates is included in the diagram. It will later be used to update the tool's definition.

Notes:

* If you measure and configure these parameters correctly, collisions should not happen.
* The carriage's coordinates are those shown in the GUI after a succesful homing.
* For now these parameters are hardcoded here: [gcodeBuilder.py](../../protocol2gcode/gcodeBuilder.py). In the future these parameters will be set through the GUI (hopefully with the help of a wizard).
* If during calibration the tool "loading" Z coordinate is different from the tool "parking" coordinate, set this offset value in the `    z_parking_offset` slot of the configuration. If your machine is perfect, the coordinates will not differ, and the offset should be set to zero.

### Procedure

Initial conditions:

- Coordinate space correctly configured (note the current coordinates after homing look like `0,0,258`).
- Enabled steppers.
    - Send `$1=255` through Fluidd, Mainsail, SSH/`minicom`, bCNC, Arduino IDE serial monitor, etc.
    - We'll add a button for this in the GUI in the future.
    - To use `minicom`:
        1. SSH into the PI.
        2. Open a serial port using, for example, `minicom -D /dev/ttyACM0 -b 115200`.
        3. Send `$1=255` to keep the steppers on during the procedure.
        4. Send `$H` to home the machine again.
        5. Close the serial port with `Ctrl a x`, to prevent conflicts with the Python scripts.
        6. If GRBL does not respond, use `Ctrl+X` to force a soft-reset.

![01-manual_control.png](./images/01-manual_control.png)

Procedure:

1. Manually remove any tools, and place them in their corresponding parking posts.
2. Open the GUI, load any workspace, and open the "Manual Control" menu (a.k.a. the _joystick_).
3. Home the machine.
4. To get the tool-change coordinates, use the GUI's joystick to move the machine to the following locations, and take note of the coordinates at each step (these are shown in the "tool data" element of the GUI).
    1. Align the carriage just in front of the a tool's alignment rods, but clear of them.
        - Note the three coordinates of the current position, these will be `xyz`.
    2. Slide the carriage in, until the latch "clicks".
        - Note the Y coordinate, this will be  `y_final`.
        - During this _click_, the counterpiece should be released.
    3. Adjust the parking switch set screw such that it _almost_ triggers at this position.
        - This calibration step is **critical**. The extra travel to activate the switch after the latch clicks is very short, about 1mm or less.
    4. Slide out the carriage, untill the Y position is such that the tool is "clear" of other tools (i.e. that it cannot crash into other tools by moving along the X axis). Now obtain the value for `y_clearance`, either:
        - Measure it directly with a ruler (recommended): `y_clearance` is the tool's length along the Y axis that protrudes from the carriage. The clearance will be computed from this value and from the most extreme `y_final`, during GCODE generation.
        - Use the GUI: first note the current Y coordinate (`y_clear_tool`), then obtain the most extreme value (the minimum in this case) of the `y` coordinate you obtained earlier (`y_min`). Use the formula to calculate `y_clearance = y_clear - y_min` (i.e. subtract `y_min` from `y_clear` to obtain `y_clearance`).
    5. Slide the tool back into the parking post, until the latch "clicks" again.
        - Check that the end-stop activates only a moment after the latch clicks.
        - During this _click_, the counterpiece should be locked/retained.
    6. Slowly slide out the carriage until the magnets detach.
        - A faster speed may result in loss of motor steps.
    7. Slide out the carriage until the alingment rods slide out of the bearings in the carriage.
5. Repeat step 4 with the other tool.
        - Each set of coordinates is required to define the tool-change movements for a tool.
6. Update pipette definitions with the new information.
    - These are provisionally hard-coded at `protocol2gcode/pipetteDriver.py`.

By following the steps above, you will obtain coordinates such as these:

| Location                       | Variables   | Tool data                                         | X  | Y   | Z  |
|--------------------------------|-------------|---------------------------------------------------|----|-----|----|
| Tool Parked, carriage aligned. | x, y, z     | {position:{x:42,y:278,z:39,p:null,status:"IDLE"}} | 42 | 278 | 39 |
| Tool docked                    | y_final     | {position:{x:42,y:310,z:39,p:null,status:"IDLE"}} | 42 | 310 | 39 |
| Tool loaded                    | y_clearance | {position:{x:42,y:274,z:39,p:null,status:"IDLE"}} | 42 | 274 | 39 |

### Tests

1. In a new browser tab, open the Jupyter Lab server running on the  Pi. It is available at port `8888`.
2. Open the `examples.ipynb` notebook.

![jupyter.png](./images/jupyter.png)

## Pipette homing{pagestep}

### Rationale

The initial state of a pipette is assumed to be at the first stop (when it's shaft is fully depressed, but not further than the start of the second stop).

From this position the pipette is ready to load volume.

What we need to find is the distance that the carriage must travel from the homing sensor, to reach the second stop (but not further).

### Procedure (CLI)

First, `ssh` into the Raspberry Pi, `cd` to the `protocol2gcode` directory, and open a `python3` console:

```bash
ssh pi@YOUR_PIs_ADDRESS # Pipettin Pi 2
cd pipettin-grbl/protocol2gcode/  
python3
```

Then run the example in the "Multi-pipette machine" section, which is copied here for convenience.

> Note: before doing this, make sure that your limit switches and stepper motors are correctly wired and configured. Otherwise things might crash into each other and some parts may be damaged.

First a simple homing test, for both pipettes:

```python
import pipetteDriver as pd

# Tune a few things, for this testing example.
pipette = pd.Pipette2(verbose=True)

# Select a p20 pipette from the builtin pipette models.
pipette.configure_pipette("p20")

# Home the pipette tool
pipette.home_pipette()

# Displace the pipette's shaft by "-2".
pipette.displace(-2, max_speed=1)

# Select a p200 pipette from the builtin pipette models.
pipette.configure_pipette("p200")

# Home the axis
pipette.home_pipette(max_speed=2, retraction_displacement=1)

# Displace the pipette's shaft by "-2".
pipette.displace(-2, max_speed=1)

# Cleanup please
pipette.close()
```

If everyng worked out, now it's time to tune the homing position, using the `retraction_displacement` argument to `home_pipette`. This parameter defines how far the motor will travel downwards after hitting the limit switch on top.

Start out with one pipette, and run:

```python
import pipetteDriver as pd

# Tune a few things, for this testing example.
pipette = pd.Pipette2(verbose=True)

# Select a p20 pipette from the builtin pipette models.
pipette.configure_pipette("p200")

# Reduce the maximum speed if you are scared
# pipette.max_speed=8

# Home the pipette tool
pipette.home_pipette(retraction_displacement=0)
```

Now:

1. Increase the value on `retraction_displacement` by some amount.
    - For example `pipette.home_pipette(retraction_displacement=2)`
    - Note: you can try with larger steps if you've already seen what "2" looks like and you are confident.
3. Re-run the `home_pipette` again with each new value.
4. Check if the pipette's shaft has reached the first stop.
    - The carriage will squeeze your fingers if you place them on top of the pipette's button. Try grabbing it from the sides, either to hold it down, or to check if there is still some distance before the first stop (after homing of course).
5. Repeat from step 1 until the pipette's shaft has reached the first stop after homing.
    - My final value was `22.9` mm for the p20 pipette tool.
    - My final value was `22.9` mm for the p20 pipette tool.
6. Change the pipette, and repeat all steps (for each tool you have).
    - In my case, I switched to the next tool with `pipette.configure_pipette("p20")`.

## Tip ejection

As for the tool-change, use the GUI to move the tool and record the coordinates at each step:

1. Place a tip on the tool manually.
2. Move to a safe Z coordinate.
3. Move to a safe Y coordinate.
4. Align the X coordinate of the tip-ejection button with the tip-ejection post.
5. Align the Z coordinate of the tip-ejection button with the tip-ejection post, such that it is just beneath the post (e.g. -1 mm).
6. Align the Y coordinate of the tip-ejection button with the tip-ejection post. The buttonw should now be directly below the post.
7. Slowly move upwards to eject the tip (NOTE: should use probing here).
8. Move downwards to release the button.
9. Move away from the ejection post up to safe Y (clear of all parked tools, should be the same as in step 3).
10. Move to a safe Z coordinate (clear of all workspace objects tools, should be lower thatn in step 2, because the tip has been ejected).

By following the steps above, you will obtain coordinates such as these:

```
G0 G90 Z131; 2) go to safe Z (above all workspace objects)
G0 G90 Y170; 3) go to safe Y (clear of any parked tools)
G0 G90 X180; 4) pre-approach X position of the 
G0 G90 Z127; 5) pre-approach Z position
G0 G90 Y312; 6) approach the toolpost
G1 G90 Z135 F200; 7) slowly eject the tip
G0 G90 Z127; relax
G0 G90 Y170; move away from the ejection post up to safe Y (parked tools)
G0 G90 Z131; go to safe Z (workspace tip box)
```

These values were written down by hand, by moving the tools with the GUI and registering the machine's position at each of the key positions.

> Note: if the pipette is not correctly fastened to the adapter, you will notice at this step. As the tip ejector starts making force, the whole tool will twis if the pipette is loose. To correct this, make sure the bottom piece holding the pipette is very tight.

## Platform position{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

* XY and Z coordiantes.
* Offsets for each tool.

Platforms are virtual objects with properties representing physical objects in the workspace, such as tip boxes and tube racks.

Repeat this calibration each time the platforms change position.

### XY-calibration

For any platform (tips, tubs, plates, etc.):

1. Home the machine XYZ.
2. Move the robot to the content at corner of the platform, and lower the pipette until it is a few millimeters over the objects (tips or tubes).
3. Adjust the physical platform such that it center point is precisely aligned with the pipette's shaft. 
4. Move the pipette to the content at the oposite corner, and repeat the alignment (previous step). Be careful, you may lose alignment of the other corner in the process.
5. Just in case, repeat these steps once again.

### Z-calibration

Usage height calibration.

#### Tip box

1. Home the machine XYZ.
2. After the XY calibration, lower the pipette until the red LED of the tip sensor turns off.
3. Register the new height shown in the coordinates.
4. Edit the tip box platform properties, setting `defaulBottomPosition` and `defaultBottomPosition` equal to that value.

![tip_rack.svg](./images/tip_rack.svg)

#### Tip offset

1. Home the machine's XYZ.
2. Place a tip on the pipette (until the red LED of the tip sensor turns off).
3. Move the pipette over some stiff object, and lower it until it barely touches it.
4. Register the Z position shown in the panel (`Z_tip`).
5. Move the pipette up, remove the tip, and repeat step 3.
6. Register the Z position shown in the panel (`Z_shaft`).

Note: this information is useful in general, but it is not currently used by the Tube rack path calculations.

![tube-tip_offsets.svg](./images/tube-tip_offsets.svg)

#### Tube rack

1. Home the machine's XYZ.
2. Place the tube rack with a tube in one of the slots.
3. Place a tip on the pipette (until the red LED of the tip sensor turns off).
4. Move the pipette over the tube, and lower it until the tip is barely above the bottom, but does not touch it (if it does, raising the pipette 0.5 mm should be enough).
5. Register the Z position shown in the panel.
6. Edit the tube rack platform properties, setting `p200LoadBottom` equal to the registerd value (add 0.5 if unsure).

Note: this is the simplest way to callibrate pipetting height. Using the tip offest will surely help with other platforms.

![tube_rack.svg](./images/tube_rack.svg)


## Tool XY-offsets

First, you must calibrate the position of items with respect to the a "first" tool, to which you will assign zero offset.

Other tools will have offsets with respect to that one.

Procedure:

1. Place reference object on the workspace, which you can align precisely to the pipette's point (or tip if placed).
2. Align the first tool to the object using the GUI's joystick.
3. Record the XY coordinates.
4. Remove the first tool and place the second tool (manually).
5. Align the second tool to the object using the GUI's joystick.
6. Record the XY coordinates.
7. Calculate the difference between the first and the second set of coordinates, to obtain the second tool's offset.

- [ ] TODO: the Z coordinate offset will be discussed later. Each platform will have a specific "default load bottom" parameter, associated with each pipette. Also the tip lengths will have to be measured. See the next section.

Use the recorded coordinates to calculate the offsets of the second tool (p20) using the first tool (p200) as a reference:

```python
cal = {"p200": {"x": 168.875,"y": 73.875,"z": 25.91}, 
       "p20": {"x": 169.875,"y": 80.875,"z": 50.98}}

offset_x = cal["p20"]["x"] - cal["p200"]["x"]
offset_y = cal["p20"]["y"] - cal["p200"]["y"]

print("x: ", offset_x)
print("y: ", offset_y)
```

> Note that we subtracted from the second set of coordinates. This is because the GCODE is built for the coordinates of the first tool, and the offsets are added just before saving each command.

## Tool Z-offsets

Each platform has a top and bottom attribute, which we must calibrate for each pipette.

Editing platforms for multi-tool support:

Keep in mind that the only platforms that are currently correcly configured for the new multi-pipette software are:

* `5x16_1.5_rack`
* `200ul_tip_rack_MULTITOOL`

The rest will probably fail, because they lack the necessary attributes.

It's a matter of updating the rest to change some parameters so that they have a value for each pipette.

In a tip rack we need something like:

```json
    "defaultLoadBottom": {
        "p200": 25,
        "p20": 4
    },
```

In a tube rack we need something like:

```json
    "defaultBottomPositionTools": {
        "p200": 29,
        "p20": 9
    }
```


## Volumetric calibration{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

* Measure the diameter of the pipette shafts.
* Precision scale.
* Tubes.
* Calibration protocols.

# Congrats!

This is really _it_! You now are ready to run pipetting protocols :3

You may:

- Return to the maintenance guide: [02_maintenance_guide.md](../02_maintenance_guide.md)
- Proceed to the user guide: [01_user_guide.md](../01_user_guide.md)

