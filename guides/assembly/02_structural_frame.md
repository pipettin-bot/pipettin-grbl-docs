# Build the structural frame

{{BOM}}

>i **Note**:
>i Under construction!

Briefly, throughout this stage you will:

* Assemble a support structure for the XYZ parts, using 2020 aluminioum profiles
* Add the motors to the Y-axis bases.
* Add the GT2 transmission belts to the CNC frame.

Some images for reference:

![profile_lengths.png](./02_structural_frame/images/profile_lengths.png)

> Expected geometry.

![assembled_frame.png](./images/assembled_frame.png)

> Expected result, with the CNC frame already on top.

>i **Note**:
>i If you already have a cartesian 3-axis CNC frame (and know what you re doing) you can use it instead :)

## Gather parts I{pagestep}

Parts list:

* [50 cm 2020 profile](2020_50cm){Qty:6}
* [46 cm 2020 profile](2020_46cm){Qty:1}
* [20 cm 2020 profile](2020_20cm){Qty:4}
* [corner_connectors.md](./02_structural_frame/corner_connectors.md){Qty:16}
* 4 slider nuts (unless you are using T bolts).

![parts.png](./02_structural_frame/images/parts.png)

>i **Note**:
>i I recommend finding a profile supplier that will cut the profiles for you. The DIY way with an angle grinder results in rougher edges.

>i **Note**:
>i If you use "hidden" corner connectors, you may need to replace the prisoner screws with short M4 screws. In my case, the prisoner screws' hex slot was strange, and did not fit any metric or imperial keys.

>i **Note**:
>i You may skip the need for corner connectors / brackets by precisely drilling holes and tapping threads into the aluminium profiles. This is sometimes called a "blind joint". See https://www.youtube.com/watch?v=2dvbn0rWA60 and https://www.youtube.com/watch?v=2dvbn0rWA60.

## Assemble the structure{pagestep}

Examine the images below carefully, and choose where to start binding the aluminium profiles.
This might depend on what binding system you have, and there are several.

For example:

- Types of brackets:
    - Corner brackets (regular or "hidden").
    - Tapped profiles.
- Types of slider nuts:
    - Non-removable (regular).
    - Removable (by turning in counter-clockwise direction).
    - Simple M4 nuts will also work, and can easily be inserted or removed.

Here are some hopefully helpful videos about this step:

* My process: [Pipettin GRBL - Structural frame (v2)](https://photos.app.goo.gl/j5QkUkDRawdvSw6b8). I made a bunch of mistakes in this build; do not use these videos as a step-by-step reference.
* Hidden corner connector (AKA inner steel brackets): [https://www.youtube.com/watch?v=GQqsscvo9Q0](https://www.youtube.com/watch?v=GQqsscvo9Q0)
* External angle connector: [https://www.youtube.com/watch?v=j7pMMNiNxNE](https://www.youtube.com/watch?v=j7pMMNiNxNE)
* Alternative with no corner connectors: [https://www.youtube.com/watch?v=2dvbn0rWA60](https://www.youtube.com/watch?v=2dvbn0rWA60)

>i **Note**: if you forget to insert the slider nuts before binding the profiles, you may eventually fin yourself repeating these steps several times. To avoid this, you can use the "Drop In" kind of  nut (that can be inserted from the sides _and from above_).

Suggested procedure:

1. Start with the bottom side of the structure first (the "H", marked in yellow below), because it's easier to build upon this later on.
2. Add the vertical profiles to the base.
3. Assemble the top "square" separately.
    - **Important:** if you are using slider nuts instead of "T bolts" and regular nuts, you **must** insert some of slider nuts before assembling the structure (marked in magenta below).
4. Join all parts.

![profile_lengths-startwith.png](./images/profile_lengths-startwith.png)

> Start with the lower side of the structure (yellow). Remember to insert slider nuts in advance (magenta) before fixing the topside profiles together.

![insert_slider_nuts_before_1.png](./02_structural_frame/images/insert_slider_nuts_before_1.png "insert_slider_nuts_before_1.png")

> Rear side of the structure with Y axis bases: note that after the assembly it would be impossible to insert a common slider nut into the middle-front profile (blue), because the slot is blocked on both ends by the other profiles (red).

![insert_slider_nuts_before_2.png](./02_structural_frame/images/insert_slider_nuts_before_2.png)

> Detail of the rear side with a Y axis base. Note that after the assembly it would be impossible to insert a common slider nut into the profile on the left, because the slot is blocked on both ends by the other profiles.

## Gather parts II

Parts list:

* 8 [Slider nuts](slider_nut.md){Qty:8} (or T bolts).
* Nuts, screws and washers (Qty: 8)

## Fix the CNC frame on top{pagestep}

For this purpose we need some slider nuts. A few of them must have been inserted in the previous step (magenta circles in the images above).

Here are some hopefully useful videos on how to use the slider nuts:

* Steel nut and a 3D-printed alternative:
    * [https://photos.app.goo.gl/54EyvL5kwkT9ddQG8](https://photos.app.goo.gl/54EyvL5kwkT9ddQG8)
    * [https://photos.app.goo.gl/i1uzah4vXxUdBS7K8](https://photos.app.goo.gl/i1uzah4vXxUdBS7K8)
* Tightening test: [https://photos.app.goo.gl/Anq1K4i2Fwr7ctuP8](https://photos.app.goo.gl/Anq1K4i2Fwr7ctuP8)
* Fixing the printed part: [https://photos.app.goo.gl/JkJ6oDsPATcsodpK7](https://photos.app.goo.gl/JkJ6oDsPATcsodpK7)

1. Place the CNC frame such that the four Y-axis base pieces are the orientation shown below.
    - Note: only the Y-axis bases are displayed for clarity, the rest of the CNC frame is not shown but must have been already assembled.
    - Advice: it might be easier to the add the four corner-most slider nuts before, and then _sliding_ the two nuts on one side into the profiles, until it is possible to slide the other two, on the other side. Then slide in the opposite direction until the parts are aligned.
2. Fix each Y-axis base to the aluminium structure by fastening the 8 screws into the corresponding slider nuts. If the mounting holes on the base are too wide, add appropriately sized washers.

![fix_Y_axis_bases_annot.png](./02_structural_frame/images/fix_Y_axis_bases_annot.png)

> Correct orientation of the Y-axis bases. The red circles mark the points where the printed parts are fastened to the frame, using slider nuts, washers, and screws.

# Congrats!

By completing these steps you have assembled the structural frame and attachd the CNC frame on top.

You may return to the assembly guide to continue: [03_assembly_guide.md](../03_assembly_guide.md)

It should look like this:

![result.png](./02_structural_frame/images/result.png)

> Final result. Note: steel rods, belts and other parts of the transmission system are not drawn.

# Design notes

The main interactions of this frame are:

1. Points of fixation for the Y axis bases, which support the entire CNC frame (marked in cyan below).
2. Points of fixation for the tool-posts (marked in red below).
3. Points of fixation for the tip-ejection post (marked in yellow below).
4. Support and align the baseplate (for now it is just sitting on it, [see issue 48](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/48)).

![assembly3_tooclhanger-alu_frame_interactions.png](./images/assembly3_tooclhanger-alu_frame_interactions.png)

> This is the full model. Take it easy, you'll get there soon enough :)

# List of to-do's

Due improvements for this page:

- [ ] Agregar el truquito del otro perfil corto para alinear cosas.
- [ ] b286: _why not using photos here as well? is there a reson or it's just because this is what you have?_.
    - Will do!



