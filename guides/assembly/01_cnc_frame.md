# Build the CNC frame

{{BOM}}

>i **Note**:
>i Under construction!

![06_XYZ_axis_final.png](../assembly/01_cnc_frame/images/06_XYZ_axis_final.png)

> Expected result.

## Design notes

Machine axes overview:

* XY axes are belt driven for speed.
* We used steel rods and linear bearings for the XY axes, but V-slot would probably work just as well.
* Z axis is driven by a lead-screw for strength (essential for tip placement).
* The pipette axis is driven by a lead-screw for precision (essential for pipetting).

>i **Note**:
>i If you already have a cartesian 3-axis CNC frame (and know what you re doing) you can use it instead, as long as it is driven by GRBL.

## 3D Print the parts{pagestep}

These parts must be 3D printed. Together with the rods and bearings, the parts make up the CNC's "mechanical" frame for the X, Y and Z axes.

> _Note_: each STL may contain several parts. If at any time you need to print the parts separately, you can easily [split the model](https://help.prusa3d.com/article/split-to-objects-parts_1751) using the [Prusa Slicer](https://www.prusa3d.com/page/prusaslicer_424/) program. 

Follow these guides:

1. [01_cnc_frame/01_print_XY_axes.md](01_cnc_frame/01_print_XY_axes.md){step}.
2. [01_cnc_frame/03_print_Z_axis.md](01_cnc_frame/03_print_Z_axis.md){step}.

## Assemble the CNC frame{pagestep}

Using the 3D printed and structural parts, assemble the frame by following these guides:

1. [01_cnc_frame/02_assemble_XY_axes.md](01_cnc_frame/02_assemble_XY_axes.md){step}.
2. [01_cnc_frame/04_assemble_Z_axis.md](01_cnc_frame/04_assemble_Z_axis.md){step}.
3. [01_cnc_frame/05_assemble_cnc.md](01_cnc_frame/05_assemble_cnc.md){step}.

## Congrats!

By completing these steps, you have made the [CNC frame]{output, Qty:1}.

Continue by returning to the parent step: [03_assembly_guide.md](../03_assembly_guide.md)

# List of to-do's

Due improvements for this page:

- [ ] Add printer calibration models. This can prevent users wasting plastic, or spending too much time in "post printing". The OpenFlexure project has a nice example on how to do this. Look it up.

