# Wire the electronics

Briefly, throughout this stage you will:

* Attach limit switches, wire motors, setup the Arduino with GRBL and a CNC shield, configure the firmware, and run basic tests.
* Manage cables.
* Set up the Rapsberry Pi 4 and wire the remaining connections (USB and GPIO).
* Make the electronics case.
* Basic sofware and movement tests.

{{BOM}}

>i **Note**:
>i Under construction!

![circuit_diagrams.svg](./03_electronics/images/circuit_diagrams.svg)

> Connections diagram.

![enclosure.png](./03_electronics/images/enclosure.png)

> Enclosure (version 2). This version is meant to have a RPi model B (left side), a _bare_ CNC shield (at the center, with no controller), and the Arduino UNO with the CNC shield on top (right side). There are mounting holes for teo "30x30" cooler fans. The sides are T-slotted, and will fir M3 screws and nuts.

<!-- 

![electronics_v2.png](./03_electronics/images/electronics_v2.png)

> Electronics enclosure (version 1). Parts: (1) power switch bewteen the PSU's 24V and the Pololu/CNC shield inputs; (2-3) 30x30 cooling fans; (4) CNC shield v3.0 clone; (5-6) A4988 drivers for the pipette steppers; (7) extra board where tip probes, pipette endstops, and parking endstops are connected; (8) Raspberry Pi 2; (9) behind these cables is the reset crictuit for the Arduino UNO. 

-->

![electronics_v3.jpg](./03_electronics/images/electronics_v3.jpg)

> Electronics mounted on the enclosure: (1) An Arudino UNO + CNC shield operate the XYZ axis of the CNC machine; (2) A bare CNC shield with 2 drivers controls the pipettes, and controled directly from (3) the Pi's GPIO header; (4) a small circuit to trigger a hard-reset on the Arduino UNO, from the Pi's GPIO (needed because of the logic voltage difference: 5v vs 3.3V).

![electronics_v3_overview.jpg](./03_electronics/images/electronics_v3_overview.jpg)

> Note: capacitors for mechanichal and optical endstops have been added as close to the sensor pins as possible (i.e. on the Pi or Arduino side of the cables). Most of the times they were soldered on the cables directly, right after the connectors, others were added to the intermediate connector board (top right of the picture).

## Gather parts{pagestep}

* Multimeter.
* 24V 10A power supply.
* Power switch.
* Cable sleeves.
* Controllers:
    * 2x CNC shield.
    * 1x Arduino UNO.
    * 6x A4988 drivers.
    * 1x Raspberry Pi, with micro-SD card, and power supply.
* Cables and wires:
    * 1x USB cable.
    * Cables for motors and endstops.
    * Jumper wires.
    * Jumper pins.
    * Thicker wires for power transmission.
* 2x 30x30 cooler fans (24V).
* 8x NO mechanical end-stops.
* 2x NC opto-endstops.
* 1x 2N2222 transistor.
* Resistances.
* 4x 4.7uF capacitors.
* Printed parts (list below).

## Printed parts{pagestep}

Print:

* 1x set of endstop holders: ![](./03_electronics/models/endstop_holders.stl)
* 150x cable carrier link modules: ![](./03_electronics/models/cable_carrier_linkmodule.stl)
* Cable carrier base modules: ![](./03_electronics/models/cable_carrier_basemodules.stl)
* 1x CNC shield holder: ![](./03_electronics/models/cnc_shield_holder-cnc_shield_holder.stl)
* 2x mounting parts for 2020 (to fix to a captive nut on a 2020 profile) ![](./03_electronics/models/enclosure-tope_para_fijar_a_perfil.stl)
* 1x Power switch mount + 4x slider nuts ![switch_box-4x_slider_nuts.stl](./03_electronics/models/switch_box-4x_slider_nuts.stl)

## Component modifications{pagestep}

>i **Note**: These steps can be skipped if you are using the newer version of the robot, which replaced GRBL with "Klipper".

1. Add noise filtering capacitors to the normally closed (NC) end-stops / limit-switches.
    - Add the capacitors between the signal pin and ground.
    - Add it to the microcontroller-end of the cable (i.e. on the end closest to the connector plugged to the Arduino or Raspberry Pi).
    - Use a wire stripper to peel an intermediate section of the cable (without cutting it), exposing the wires. Now solder the capacitor.
    - Finally add some hot glue to isolate the exposed solder points.
2. Parallel endstop wiring.
    - Use a wire stripper to peel an intermediate section of the first cable (without cutting it), exposing the wires.
    - Cut the wires on the second cable, at the same place as the first cable, thereby removing the connector.
    - Peel the second cable, and solder each wire to the corresponding wire on the first set of cables (i.e. by color).
    - Finally add some hot glue to isolate the exposed solder points.
    - Cut the "NC" pin on both switches (see image below). Make sure that their connections are properly severed (e.g. move the ends of the severed pins away from the other).


![endstop_NC_snipping.png](./03_electronics/images/endstop_NC_snipping.png)

> NC switch snipping.

## Wire up: Logic{pagestep}

TO-DO:

* Attach end stops.
* Solder the reset circuit.
* Solder the fan power circuit.
* Solder the tool limit sensors board.
* General connections: USB, GPIO/Pins, motors, end-stops.

## Mount components and enclosure{pagestep}

TO-DO:

1. Add 4 stepper drivers to the first CNC shield, and mout it on the Arduino UNO.
2. Attach the Arduino UNO + CNC shield (v3) to the base of the enclosure, using the mounting holes and M3 screws and nuts.
3. Add 2 stepper drivers to the second CNC shield.
4. Attach the second CNC shield to the CNC shield holder, and then to base of the enclosure.
5. Attach the Raspberry Pi to the base of the encolusure. Use M3 nuts as spacers, or a couple M3 "towers".
6. Attach the reset circuit to the enclosure base using hot glue.
7. Attach the fan power circuit to the enclosure base using hot glue.
8. Attach the tool limit sensors board to the tip-ejection post using hot glue.

> _La caja! la caja!_

## Cable management{pagestep}

TO-DO:

* Put all cables into place. Check that they are long enough.
* Tuck the cables into places, using cable-carriers, helicoidal sheaths, and the v-slots in the strucural frame.

## Wire up: Power{pagestep}

TO-DO:

* Install the 24V PSU.
    * Adjust the voltage to around 23.5 V if possible (just in case, this is very near the maximum rating of some stepper drivers).
* Wire the power switch and install it using the power switch mount, and slider nuts.
* Do not turn anything on yet, wait until you finish the "setup" guide.

## Congrats!

By completing these steps you have wired and cable-managed the electronics of the robot.

You may return to the assembly guide to continue: [03_assembly_guide.md](../03_assembly_guide.md)

TODO <!-- Include pic -->

# Design notes

The main interactions of the electronics are:

* Uncountable (?)
* IOU

Designed to be cheap, modular, and functional.

# List of to-do's

Due improvements for this page:

- [ ] Write the "design notes" section.
- [ ] Write detailed steps with pictures for each section (see TO-DOs above).
- [ ] Write the "klipper" version of this guide.
