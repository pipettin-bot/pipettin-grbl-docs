# Build the CNC frame

{{BOM}}

>i **Note**:
>i Under construction!

## Design notes

Machine axes overview:

* XY axes are belt driven for speed.
* We moved from steel rods and linear bearings to V-slot wheels.
* Z axis is driven by a lead-screw for strength (essential for tip placement and eyection).
* The pipette axis is driven by a lead-screw for precision (essential for pipetting precisely).

>i **Note**:
>i If you already have a cartesian 3-axis CNC frame (and know what you re doing) you can use it instead, as long as it's controller board can be driven by Klipper (or your own control software).

## 3D Print the parts{pagestep}

These parts must be 3D printed. Together with the rods and bearings, the parts make up the CNC's "mechanical" frame for the X, Y and Z axes.

> _Note_: each STL may contain several parts. If at any time you need to print the parts separately, you can easily [split the model](https://help.prusa3d.com/article/split-to-objects-parts_1751) using the [Prusa Slicer](https://www.prusa3d.com/page/prusaslicer_424/) program. 

Follow these guides:

1. To-do

## Assemble the CNC frame{pagestep}

Using the 3D printed and structural parts, assemble the frame by following these guides:

1. To-do

## Congrats!

By completing these steps, you have made the [CNC frame]{output, Qty:1}.

Continue by returning to the parent step: [03_assembly_guide.md](../03_assembly_guide.md)

# List of to-do's

Due improvements for this page:

- [ ] Add printer calibration models. This can prevent users wasting plastic, or spending too much time in "post printing". The OpenFlexure project has a nice example on how to do this. Look it up.

