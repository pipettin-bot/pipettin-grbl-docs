# Assemble the tools

Briefly, throughout this stage you will:

* Build the two pipette tools (using the p20 and p200 pipettes from Gilson).
* Build the tool posts.
* Attach the tip-ejection post.

![p20_tool_assembled.jpg](./images/p20_tool_assembled.jpg)

> Assembled tool.

![model.png](./images/model.png)

> Model. Many parts are missing from this model: the p20 pipette, the springs, opto-stop sensor, all screws and nuts, linear bearings, THSL rod and nut, mechanical end-stop, and stepper-rod copling.

![assembly3_tooclhanger_tools.png](./images/assembly3_tooclhanger_tools.png)

> Tool adapters, tool parking-posts, and the tip-ejection post (in colour) mounted on the frame (grayscale).

{{BOM}}

>i **Note**:
>i Under construction!

## Print and gather parts{pagestep}

> The following quantities are for two pipette tools, tipically one p200 and p1000. Only the "tip probe" parts differ. If you want to use a different pipette set, make sure to print the corresponding probes.

1. Gather parts:
    - Gilson [p200_micropipette](p200_micropipette){Qty:1}
    - Gilson [p20_micropipette](p20_micropipette){Qty:1}
    - *The following quantities correspond to 2 tool posts*:
    - [30_cm_2020_profile](30_cm_2020_profile){Qty:2}
    - [slider_nut](30_cm_2020_profile){Qty:4}
    - [mini_latch](mini_latch){Qty:2}
    - *The following quantities correspond to 2 tip probes*:
    - [opto_enstop](opto_enstop){Qty :2}
    - [conical_spring_0.4Dw_11.8D1_8.5D2](conical_spring_0.4Dw_11.8D1_8.5D2){Qty:8}
    - *The following quantities correspond to 2 pipette adapters*:
    - [125mm_8mm_rod_chamfered](125cm_8mm_rod_chamfered){Qty :4}
    - [185cm_8mm_rod](185cm_8mm_rod){Qty :4}
    - [limit_switch_modified](limit_switch_modified){Qty :4}
    - [lm8uu_bearing](lm8uu_bearing){Qty:8}
    - [small_nd_magnets_2.5h_6od](small_nd_magnets_2.5h_6od){Qty:16}
    - [625zz_bearing](625zz_bearing){Qty:8}
    - [nema17_motor](nema17_motor){Qty:2}
    - [thsl_leadscrew_8OD_50L](thsl_leadscrew_8OD_50L){Qty:2}
    - [thsl_nut_8OD](thsl_nut_8OD){Qty:2}
    - [5mm_8mm_motor_coupling](5mm_8mm_motor_coupling){Qty:2}
    - M3 screws and nuts.
    - M2 screws and nuts.
2. Print the [tip probes](tip_probe){Qty:2}:
    - Models: [gilson_tip_probe_p20_and_p200.stl](./models/gilson_tip_probe_p20_and_p200.stl) (single STL for one p200 and one p20).
    - Models: [gilson_tip_probe-p1000_probe_assembly.stl](./models/gilson_tip_probe-p1000_probe_assembly.stl) (print this once for once for one p1000).
    - Use 0.2 mm layer height.
    - This uses 2.3 [printer hours](01_cnc_frame/tools.yaml#print_time){Qty:2.3}
3. Print two [pipette adapters](pipette_adapters){Qty:2}
    - Models: [glson_pipette_adapter.stl](./models/glson_pipette_adapter.stl) (print this twice for two pipettes).
    - Use 0.2 mm layer height.
    - This uses 8.5  [printer hours](01_cnc_frame/tools.yaml#print_time){Qty:8.5}

![](./models/gilson_tip_probe_p20_and_p200.stl)
> Tip probes: p200 amd p20.

![](./models/gilson_tip_probe-p1000_probe_assembly.stl)
> Tip probes: p1000.

![](./models/glson_pipette_adapter.stl)
> Pipette adapters.

## Assemble the tip probes{pagestep}

Use the images below as a reference for part numbers, and as supporting information to understand the instructions.

1. Press-fit the stalks (parts with number 2, in ocre/yellow) and the ring part (number 3, red with transparency).
2. Insert the _flaps_ of part 1 (the smallest, red part on the left) into the slots of both stalks.
3. Place two M2 screws into the holes (cyan arrows, left). The tips of the screws must not protrude on the other side (i.e. they must be flush with part number 1).
4. Slide two conical springs into the stalks (red arrows), such that the wider side faces away from part 1 (and ventually towards part 5).
5. Slide the stalks into part number 5. Note that on this end, neither the stalks nor the slots are symmetric.
    - **important**: try sliding the stalks in and out, to check that they slide freely. If they do not, sand them flat and/or add a lubricant (such as fine graphite powder). Also check the slots on part 5 for any imperfections.
7. Slide two conical springs into the stalks (green arrows), but this time the wider side must face towards part number 5 (and part 1 as well).
8. Compress the springs slightly, and insert longer M2 screws into the holes of the stalks (green and red arrows), such that there are two on each stalk, and two on each side of part number 5.
    - The M2 screws are meant to compress the spings on each side of part number 5, such that the stalk now "floats" elastically around part 5.
    - For example: the blue arrow shows the position of an M2 screw, and the magenta line shows where the corresponding spring would be.
9. Place part number 4 agains the side of part 5, aligning the slots on part 4 with the holes on part 5. Insert M2 nuts into the slots on part 5 (magenta arrow) and screw part 4 to part 5 by inserting an M2 screw.
10. Finally, add M3 nuts and screws to the central section of part 5; do not fasten them yet, they will fasten the probe to the pipette's shaft.
11. Fix the opto-stop to part 4, using two M2 nuts and screws.

Done!

![probe.png](./images/probe.png)

> Model

![p20_tool_assembled_probe.jpg](./images/p20_tool_assembled_probe.jpg)

> Assembled probe. Note that the M3 nuts and screws were not used in this case, as the probe could be pressure-fitted to the shaft with a small amount of force.

## Attach the tip probes to the pipette{pagestep}

Use the image above as a supporting reference.

1. Remove the tip ejector from the micropipette.
    - Note: some of them are simply pulled away, others have a lock. Check by pressing the tip ejection button on the pipette, and inspect the upper end of the tip ejector.
2. Slide in the parts in the following order:
    - Slide in part 5 of the assembled tip probe into the pipette's shaft until it stops.
    - Slide in the tip-end of the ejector.
    - Slide in part 1 of the assembled tip probe.
    - Place the other end of the ejector back into the pipette. Check that it is well placed.
    - Fasten the screws on part 5 only if necessary. You may otherwise break the shaft.
3. Part 1 of the tip probe must "float" at a distance of ~2 mm in front of the tip ejector.
    - To achieve this, you need to try different positions for the M2 screws.
    - They cannot be too close, to allow the probe to slide freely.
    - They cannot be too far, because the springs must always be tensioned.
    - Remove and reinsert the screws on either side until you achieve the desired result (see picture above).
4. Align the opto-stop: slide part 4 until the wider end of the wider stalk is just before the slot on the opto-stop.
    - Proper alignment will be done later on, once the opto-stop is powered.

## Assemble the adapter{pagestep}

Prepare the stepper base:

1. Add M3 captive nuts to the stepper base (yellow part, green arrows).
2. Insert two short M3 screws until they bind their nut (magenta arrows).
3. Find the stepper back-plate (blue-transparent part) and fasten it to the stepper base using two M3 screws.
4. Add four neodymium magnets to the back-plate, two on each side (red arrows).
    - Make sure that they are in the same (magnetic) orientation.
    - Push them into the slots until they are flush with the part.

![stepper_base.png](./images/stepper_base.png)

6. Fix the stepper's shaft to a 5-8 mm coupler.
7. Screw the stepper base (yellow part) to the stepper motor (use the next picture as a reference).
    - The steppers connector must face _sideways_, as shown in the image below.
    - Use only 3 of the four available holes (red circles).
8. Screw a long, 40 mm screw into the remaining hole (red arrow).
9. Fix the short THSL lead screw to the motor's coupler.

![model.png](./images/model_annotations.png)

>! **Warning**
>!
>! Incomplete instructions from now on… Work in progress!

2. Prepare the carriage.
    1. Attach the endstop.
    2. ...
4. Prepare the main adapter part.
    1. ...
6. Prepare the support adapter part.
    1. …
7. Add the smooth rods and fix them

## Fix the pipette to the adapter{pagestep}

TODO <!-- para una pipeta, y despues repetir para la otra -->

1. Cut a few pieces of cardboard (about 2 cm by 8 cm). You may also use folded paper.
2. Present the pipette on the adapter, with the volume dial facing the adapter, and the tip ejection button facing outwards.
3. Gently push the pipette into the adapter.
4. Align the pipette, such that the shaft is parallel to the carriage's movement axis.
5. If the lower part of the adapter does not touch the pipette, add the cardboard pieces to fill the gap entirely.
6. Use the printed part and screws to fasten the pipette to the lower part of the adapter. This will fix the pipette in place completely.

## Assemble the tool-posts{pagestep}

* Assemble.
    * Place bearings on the printed part.
    * Fix the mini-latch to the printed part.
    * Add the screw for the parking endstop.
    * Attach the printed part to the aluminium profile.
    * Place one M3 slider nut on the _outer_ V-slot of the aluminium profile (i.e. on the side facing away from the tool and the rest of the machine). This will be one attachment point for the electronics case. Skip this if using the easily removable kind of nut.
* Attach.

## Attach the tip-ejection post{pagestep}

* Assemble
* Attach
* Include cabelcarrier base modules

## Congrats!

By completing these steps you have assembled the two pipette adapter tools of the robot.

You may return to the tools guide to continue: [04_tools.md](../04_tools.md)

TODO <!-- Include pic -->

# Design notes

The main interactions of the tools are:

- Tool-changer interface.
- Platform items one the workspace/baseplate (i.e. stuff on the tobot's table).
- Controller electronics.

# List of to-do's

Due improvements for this page:

- [ ] Add pictures for each step.
- [ ] Write the "design notes" section.


