# Electronic micropipette

## Print and gather parts{pagestep}

## Assemble the tool-posts{pagestep}

## Attach the tip-ejection post{pagestep}

## Congrats!

By completing these steps you have assembled the two pipette adapter tools of the robot.

You may return to the tools guide to continue: [04_tools.md](../04_tools.md)

TODO <!-- Include pic -->
