# Assemble the tools

Briefly, throughout this stage you will build pipette tools. Two options are available:

- Gilson pipette adapters.
- Custom electronic multi-tip micropipetes.

## Adapter for manual pipettes{pagestep}

Adapters for Gilson micropipettes, for three sizes: 20 uL, 200 uL, and 1000 uL.

These include a "tip probe" addition to know when a tip is placed at the correct depth.

Follow the guide: [04_gilson.md](./04_tools/04_gilson.md){step}

![p20_tool_assembled_small.jpg](./04_tools/images/p20_tool_assembled_small.jpg)

## Electronic micropipette{pagestep}

A custom micropipette, fitting the three classic tip sizes: 20 uL, 200 uL, and 1000 uL.

Made with stainless steel dowel-pins, a polyfluorocarbon "tophat" seal, and a fastening o-ring.

Follow the guide: [04_multitip.md](./04_tools/04_multitip.md){step}

![bruno_small.png](./04_tools/images/bruno_small.png)

> Designed by Bruno. H. Serrentino.

## Congrats!

By completing these steps you have assembled the pipette tools of the robot.

You may return to the assembly guide to continue: [03_assembly_guide.md](../03_assembly_guide.md)

TODO <!-- Include pic -->

# Design notes

The main interactions of the tools are:

- Tool-changer interface.
- Platform items one the workspace/baseplate (i.e. stuff on the tobot's table).
- Controller electronics.

# List of to-do's

Due improvements for this page:

- [ ] Add pictures for each step.
- [ ] Write the "design notes" section.


