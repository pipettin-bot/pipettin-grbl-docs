# Corner Connectors

We used the "hidden" style connectors.


>i **Note**:
>i Note: you may need to replace the prisoner screws with short M4 screws. In my case, the prisoner screws' hex slot was strange, and did not fit any metric or imperial keys.

![corner_connector.jpg](./images/corner_connector.jpg)
