# Slider nuts

Either steel, or 3D-printed nuts.

## M4 steel slider nuts

![slider_nuts_metal.jpg](./slider_nuts_metal.jpg)

## M3 3D-printed slider nuts

These sliders use the thread of an M3 nut each.

![03-pasantes_2020.png](./03-pasantes_2020.png)