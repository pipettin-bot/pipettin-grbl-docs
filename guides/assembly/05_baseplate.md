# Workspace baseplate and platform curbs

>i **Note**:
>i Under construction!

{{BOM}}

<!-- 03_assembly_guide.md -->

Preview:

![pic.jpg](./05_baseplate/images/pic.jpg)
![overview.jpg](./05_baseplate/images/overview.jpg)

## Required skills and resources

As stated in the main assembly guide, you'll benefit from access to a CNC router. There are rather common, but you _can_ manage without one.

The main objective here is to drill on the baseplate:
- A regular grid of holes, for the curbs.
- Three goles for the kinematic coupling to the rest of the machine.

Your options:

1. Hire a CNC machineing service is to drill the regular grid of holes on the baseplate, and the kinematic coupling holes.
    - The baseplate should be easy to machine in a cheap laminated wood or anything fancier.
2. Drill your own holes.
    - The precision requirements for the baseplate are not too high. Keeping stuff square is the only requirement.
    - Good use of a ruler, carpenter's square, and hand-held drill should suffice.
3. If you are willing to permanently attach stuff to the baseplate and the machine, you only need a plank of approprate size and some screws. Skip the rest. Check out the other versions of the baseplate [here](https://gitlab.com/pipettin-bot/pipettin-grbl/-/tree/master/models/modelos_baseplate).

## Gather the parts{pagestep}

* 1x Wood plank:
    * 50cm x 50cm.
    * 9 mm thickness.
    * Barnished.
    * Ignore the 46 cm version in pictures below.
* 3x screws, M4, round head.
* 3D-printed curbs: ![](./05_baseplate/models/curbs-baseplate_assembly3.stl)

## CNC the baseplate{pagestep}

1. Ask your local CNC service to:
    * Drill the grid: 6 mm diamter holes, 5 cm away from each other in both directions (center-to-center distance).
    * Drill holes 1 and 2 on the same edge, 10 mm from the edge; they must align with the v-slot on the lateral profile (left image).
    * Drill the hole 3 on the opossite edge, 30 mm from the edge; it must align with the v-slot on the _crossing_ profile (right image).
    * The precision of this step is critical to the general alignment of objects; the directions of the grid must be parallel to the CNC's XY axis.

Example:

![second_version_model.png](./05_baseplate/images/second_version_model.png)
![3_pt_kinematic_coupling.png](./05_baseplate/images/3_pt_kinematic_coupling.png)

## Add the support screws{pagestep}

1. Add the round-head M4 screws to the holes (red arrows and numbers).
2. Place the baseplate on top of the frame. The screws must match their v-slots on their respective profiles (green arrows). Once placed, the baseplate should stay in place with no movement.
3. Adjust the screws to ensure that the distance between the baseplate and the aluminium profiles is very small but visible (e.g. ~0.3 mm).

![points.jpg](./05_baseplate/images/points.jpg)

![stability.png](./05_baseplate/images/stability.png)

## Add the curbs{pagestep}

1. Place the curbs in a suitable pattern.
2. Place some objects against the corners formed by the curbs (red segments).

![corners.jpg](./05_baseplate/images/corners.jpg)

## Congrats!

By completing these steps you have built the baseplate and attached a few basic curbs for the objects (a.k.a. *platforms*).

You may return to the assembly guide to continue: [03_assembly_guide.md](../03_assembly_guide.md)

![pic.jpg](./05_baseplate/images/pic.jpg)


# Design notes

The main interactions of the baseplate are:

* Consistent alignment to the XY axis of the CNC frame, through a kinematic coupling.
* Support for curbs and objects, through the regular grid of holes (and the fact that it is a table).
    * It must bare the force needed to place a tip.

The baseplate connects the machine's structural frame to the protocol objects (i.e. tube racks, tip racks, etc.).
It is no more than a "table" with regular holes, and _curbs_ as a way to align objects to it, easily and consistently.

A detailed explanation is available at the development repo, under [`models/modelos_baseplate/README.md`](https://gitlab.com/pipettin-bot/pipettin-grbl/-/tree/master/models/modelos_baseplate).


