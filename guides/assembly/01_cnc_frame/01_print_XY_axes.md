# XY axes 3D printed parts

{{BOM}}

>i **Note**:
>i Under construction!

## Print the parts{pagestep}

Use a [3D printer](tools.yaml#3dprinter){Cat:tool,Qty:1} to print the parts for the XY axes.

Models: [01_models.stl](./models/01_models.stl)

> _Note_: if you need to print the parts separately, you can easily [split the model](https://help.prusa3d.com/article/split-to-objects-parts_1751) using the [Prusa Slicer](https://www.prusa3d.com/page/prusaslicer_424/) program. 

![](./models/01_models.stl){color: deeppink}

> GitBuilding preview of the models.

Notes:

* The print used 13.5 [printer hours](tools.yaml#print_time){Qty:13.5}, and [PLA plastic filament](frameparts.yaml#PLA){Qty: 250 g}.
* The layer height was 0.3 mm, and some supports were used (see image below, red circles).

![XY_slicer2.png](./images/XY_slicer2.png)

> Evaluate the need for supports, specially for the parts marked in red.

Expected result:

![images/01_print_XY.jpg](images/01_print_XY.jpg)

> The models differ from the STL because they have been updated and the photograph has not. Note that I accidentally fused two of the parts while arranging them in the slicer. I later cut them apart using a hot knife. The models have been updated and you shouldn't have this problem :)

## Post-printing{pagestep}

1. Remove the 3D printing supports from the 3 pieces.
2. Check that every piece printed without problems.

![images/01_print_XY.jpg](images/01_print_post.png)

## Congrats!

You have the 3D printed the [XY printed parts]{output, Qty: 1} with cable carrier bases.

Return to the main step and continue: [01_cnc_frame.md](../01_cnc_frame.md)

# List of to-do's

Due improvements for this page:

- [ ] Link to the individual STL files, in case the models do not fit in the user's 3D-printer.

