# Prepare XY axis parts

{{BOM}}

>i **Note**:
>i Under construction!

## Gather the Y axis parts{pagestep}

1. Gather the relevant parts from the set of [XY printed parts](fromstep){Qty:1} to assemble the Y frame: 4 Y axis bases, and 2 Y carriages.

![03_base_y.png](./images/03_base_y.png)
![03_carrito_y.png](./images/03_carrito_y.png)

## Insert bearings into the Y carriage{pagestep}

1. Gather [lm8uu](lm8uu){Qty:8} bearings, and insert two of them into each of the corresponding holes in the X axis carriages.
2. Optional: if the bearings are loose and fall out, put som [scotch tape](scotch_tape){Qty:1, cat:tool} around them before insertion.

![03_carrito_y_insert.png](./images/03_carrito_y_insert.png)

![03_carrito_y_insert_tape.png](./images/03_carrito_y_insert_tape.png)

## Insert prisoner nuts and screws into the Y axis base{pagestep}

These will hold the steel rods in place, later on.

1. Find 8 [m3_nuts](m3_nuts){Qty:8}, 8 [3x8_screws](3x8_screws){Qty:8}, and the four Y axis base parts (see pic below).
2. Insert the nuts into the square slots (red). If you have some trouble with tolerances, you can try using [pliers](pliers){Qty:1, cat:tool} and a [flat_screwdriver](flat_screwdriver){Qty:1,cat:tool}. Push them in until they align with the hole for the screw (blue, orange).

![03_basey_nuts.png](./images/03_basey_nuts.png "03_basey_nuts.png")
![03_basey_nuts_01.png](./images/03_basey_nuts_01.png)
![03_basey_nuts_02.png](./images/03_basey_nuts_02.png)
![03_basey_nuts_03.png](./images/03_basey_nuts_03.png)

4. Place the screws with a [phillips_screwdriver](phillips_screwdriver){Qty:1,cat:tool}, and turn until they are just about to pass into the hole for the steel rod (magenta).
5. Repeat for all of the 4 Y axis base parts.

![03_basey_nuts_04.png](./images/03_basey_nuts_04.png)

Expected result:

![03_basey_nuts_06.png](./images/03_basey_nuts_06.png)


## Insert prisoner nuts and screws into the Y carriage{pagestep}

1. Find 4 [m3_nuts](m3_nuts){Qty:4}, 4 [3x8_screws](3x8_screws){Qty:4}, and the two Y axis carriage parts (see pic below).
2. Repeat the steps above!

Expected result:

![03_basey_nuts_05.png](./images/03_basey_nuts_05.png)

## Add X stepper to the first Y carriage{pagestep}

1. Gather a [high_torque_nema17](high_torque_nema17){Qty:1}, four [3x13_screws](3x13_screws){Qty:4}, and one of the X axis carriages prepared above.
2. Place the stepper with the connector facing "upwards" (red square, see picture below).
3. Secure the stepper using the four screws (orange circles).

Expected results:

![04_X_stepper_final.png](./images/04_X_stepper_final.png)

> Check the "upward" final orientation of the stepper's connector, relative to the other parts.

![stepper_orientation.png](./images/stepper_orientation.png)

> Check the "upward" final orientation of the stepper's connector, relative to the other parts (i.e. the green cable carrier base).

## Add pulley bases to the second Y carriage{pagestep}

1. Gather:
    - Seven [3x16_screws](3x8_screws){Qty:7},
    - two [608zz_bearing](608zz_bearing){Qty:2} (red),
    - the other Y axis carriage part from the previous step (blue),
    - the pulley bases from the XY printed parts (orange),
    - two [24mm_8mm_rod](24mm_8mm_rod){Qty:2},
    - and two [8mm_pulley_idlers](8mm_pulley_idlers){Qty:2} (from the printed parts).

Parts needed:

![04_X_pulley_bearings.png](./images/04_X_pulley_bearings.png)

2. Insert the bearings into the slots at the center of each pulley base (orange squares).
3. Insert the 24 mm rod into the pulley idler, and center it (magenta circle).
4. Place one of them on the inner side of the pulley base part in the Y carriage (cyan square). The bearing inside should now stay, without falling out.
5. Place three [3x16_screws](3x8_screws) to fix the part in place (3 red circles).
6. Insert the rod into the bearing of the remaining pulley base, and then into the bearing of the pulley attached to the Y axis carriage on the previous step (cyan dotted lines).
7. Fix the free pulley base to the Y axis carriage with four [3x16_screws](3x8_screws) (4 red circles).
    - Note: you will need to remove these screws later on, to place the GT2 belt, so you may want to adjust them only slightly.

![04_X_pulley_bearings_placed.png](./images/04_X_pulley_bearings_placed.png)
![04_X_pulley_base_placed_1.png](./images/04_X_pulley_base_placed_1.png)
![04_X_pulley_base_placed_2.png](./images/04_X_pulley_base_placed_2.png)
![04_X_pulley_base_screws_1.png](./images/04_X_pulley_base_screws_1.png)

> Note: The rod in the pictures is longer than 24 mm because it is not yet cut. I did this later, but you should prepare it beforehand.

Expected result:

![04_X_pulley_base_final.png](./images/04_X_pulley_base_final.png)

> Note: At the tim I took the picture, I had not cut the rod yet to its final length of 24 mm.

## Add GT2 belt locks to the Y carriages{pagestep}

1. Gather:
    - 8 [m3_nuts](m3_nuts){Qty:8}
    - 24 [3x16_screws](3x8_screws){Qty:24}
    - the two Y axis carriage parts from the previous step, 
    - 4 belt lock bases (red squares), and 4 belt lock lids (orange squares).
2. Screw the [3x16_screws](3x8_screws) in the bases, from the flat side (blue arros).
3. Place the lids on top (cyan squares), and secure them temporarily using the [m3_nuts](m3_nuts) (cyan circles). 
4. Now your 4 belt locks are assembled. Screw two of each to each of the the X axis carriages, fastening only the lateral screws (magenta circles). The remaining screws (yellow squares) will be fastened later on (when placing the GT2 belts).

![04_X_belt_locks_1.png](./images/04_X_belt_locks_1.png "04_X_belt_locks_1.png")
![04_X_belt_locks_2.png](./images/04_X_belt_locks_2.png)
![04_X_belt_locks_3.png](./images/04_X_belt_locks_3.png)
![04_X_belt_locks_4.png](./images/04_X_belt_locks_4.png)

Expected result:

![04_X_belt_locks_final.png](./images/04_X_belt_locks_final.png "04_X_belt_locks_final.png")

## Gather the X carriage parts{pagestep}

1. Gather the relevant parts from the set of [Z axis printed parts](fromstep) to assemble the X-axis carriage (which is also the base of the Z-axis).

![Image not found: ./images/04_carrito_Z_alt.png](./images/04_carrito_X_alt.png)

## Insert bearings into the X carriage{pagestep}

1. Gather [lm12uu](lm12uu){Qty:4} bearings, and insert two of them into each of the corresponding holes in the X axis carriages.
2. Optional: if the bearings are loose and fall out, put some [scotch tape](scotch_tape){Qty:1, cat:tool} around them before insertion.

Expected result:

![04_carrito_Z_done1.png](./images/04_carrito_X_done1.png)

## Insert prisoner nuts and screws into the X axis base{pagestep}

These will hold the steel rods in place, later on.

1. Find 12 [m3_nuts](m3_nuts){Qty:12}, 4 [3x8_screws](3x8_screws){Qty:4}, and the two X axis carriage parts prepared earlier (see pic below).
2. Insert the nuts into the square slots (red). If you have some trouble with tolerances, you can try using [pliers](pliers){Qty:1, cat:tool} or a [flat_screwdriver](flat_screwdriver){Qty:1,cat:tool} (and a [hammer](hammer){Qty:1,cat:tool} if necessary). Push them in until they align with the hole for the screw.
3. Insert the rest of the nuts "axially" into the 6 hexagonal slots (3 in each part).

Expected result:

![04_carrito_Z_done2.png](./images/04_carrito_X_done2.png)

## Congrats!

You have assembled the [XY axis parts]{output, Qty: 1}.

Return to the main step and continue: [01_cnc_frame.md](../01_cnc_frame.md)


# List of to-do's

Due improvements for this page:

- [ ] Update pictures of models.
- [ ] Fix broken part links.

