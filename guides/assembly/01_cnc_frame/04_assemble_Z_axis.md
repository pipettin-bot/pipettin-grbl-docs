# Prepare Z axis parts

{{BOM}}

>i **Note**:
>i Under construction!

## Gather the Z carriage parts{pagestep}

1. Gather the two relevant parts from the set of [Z axis printed parts](fromstep){Qty:1} to assemble the Z axis carriage parts (red arrows).

![05_carrito_Z_parts_v2.png](./images/05_carrito_Z_parts_v2.png)

## Insert bearings into the Z carriage{pagestep}

1. Gather [lm8uu](lm8uu){Qty:4} bearings, and insert two of them into each of the corresponding holes in the Z axis carriages.
2. Optional: if the bearings are loose and fall out, put some [scotch tape](scotch_tape){Qty:1, cat:tool} around them before insertion.

Expected result:

![05_carrito_Z_bearings_v2.png](./images/05_carrito_Z_bearings_v2.png)

> Note: the smaller part should have a hole through its center.

## Insert nuts and screws into the Z axis base{pagestep}

These will hold the steel rods in place, later on.

1. Find 2 [m3_nuts](m3_nuts){Qty:6}.
2. Insert the nuts into the square slots (red). If you have some trouble with tolerances, you can try using [pliers](pliers){Qty:1, cat:tool} or a [flat_screwdriver](flat_screwdriver){Qty:1,cat:tool} (and a [hammer](hammer){Qty:1,cat:tool} if necessary). Push them in until they align with the hole for the screw (blue, orange).

Expected result:

![05_carrito_Z_nuts1_v2.png](./images/05_carrito_Z_nuts1_v2.png)


## Secure the threaded rod nut into the Z carriage{pagestep}

* Find 2 [m3_nuts](m3_nuts){Qty:2} and 2 [3x30_screws](3x30_screws){Qty:2}.
* Use them to fix the [1t_thsl_nut](1t_thsl_nut){Qty:1} onto the Z axis carriage (see pic below).
 ![05_carrito_Z_thsl_nut_v2.png](./images/05_carrito_Z_thsl_nut_v2.png)

Here is an example of what the result should look like, on a previous version of the Z axis carriage:

![05_carrito_Z_thsl_nut.png](./images/05_carrito_Z_thsl_nut.png)

> Top side (left) with the THSL nut and two M3 nuts, and the underside (right) with the two screw-heads.

## Prepare the Z axis motor{pagestep}

1. Find the Z axis motor base in the [Z axis printed parts](fromstep), a [high_torque_nema17](high_torque_nema17){Qty:1}, and four [3x13_screws](3x13_screws){Qty:4}.
    - **Important**: Make sure the orientation of the motor connector is such that it is on the opposite side as the "U" on the motor base (se picture below). If not, the next step will be more complicated, or the cables will collide with the pipette tool (later on).
3. Gather 4 [3x30_screws](3x30_screws){Qty:4} and partially screw them into the other side of the motor base.
4. Screw the motor to the printed part.

![06_stepper_Z_crop.png](./images/06_stepper_Z_crop.png)

Expected result:

![06_stepper_Z_screwed.png](./images/06_stepper_Z_screwed.png)
![06_stepper_Z_X_screws.png](./images/06_stepper_Z_X_screws.png)

## Place the Z axis motor{pagestep}

1. Find the X axis carriage (which is also the Z axis base), the Z motor with its base, and the Z axis carriages. All of these were prepared in previous steps.
2. Use two 8mm rods to help align the parts before doing this: insert them into the Z axis carriages and into the holes in the Z motor base.

![06_Z_axis__motor_helper_rods.png](./images/06_Z_axis__motor_helper_rods.png "06_Z_axis__motor_helper_rods.png")

> Do not tighten these screws too much, or the 3D-printed part will break.

3. Turn the Z axis carriages slightly and place a [608zz_bearing](608zz_bearing){Qty:1} into the slot at the top of the Z axis carriages. Align them back with the Z axis motor base, so it doesn't fall out of place.

![06_Z_axis_bearing.png](./images/06_Z_axis_bearing.png "06_Z_axis_bearing.png")

4. Screw the motor base to the X axis carriage parts, using the 4 partially screwed screws in the Z motor base.
    - If you find that the screws dont easily screw into the nuts on the other side, try removing the screws completely and placing them back in. This is because the threads of the screw and nut may be "out of phase".

![06_Z_axis__motor_screw.png](./images/06_Z_axis__motor_screw.png)

> Note: in these pics the four Z motor base screws were not presented beforehand, which complicated assembly.

Expected result:

![06_Z_axis_motor_screw_final.png](./images/06_Z_axis_motor_screw_final.png)

> Note: the Z motor screws protrude a bit out of the Z axis base, this is ok. The coupling between the motor's shaft and the threaded rod also will, so the space is not wasted just because of this.

5. Place the prepared Z axis carriage, then slide in two [8mm_40cm_rod](8mm_40cm_rod){Qty:2}, and tighten the 4 set screws slightly.
    - Note: again, orientation of the Z axis carriage must be correct.

## Add GT2 belt locks to the X axis carriage{pagestep}

1. Gather:
    - 4 [m3_nuts](m3_nuts){Qty:4}
    - 4 [3x16_screws](3x8_screws){Qty:4}
    - 4 [3x30_screws](3x30_screws){Qty:4}
    - the two X axis carriage parts from the previous step, 
    - 2 belt lock bases (red squares), and 2 belt lock lids (orange squares).
2. Repeat the steps shown previously to prepare 2 belt locks.
3. Screw one belt lock to the mounting holes on each of the X axis carriage parts prepared above using the longer [3x30_screws](3x30_screws).
    - Note: make sure the belt locks have the GT2 rack facing "upward" (red squares), and that the mounting screws reach the nuts on the _other_ side of the X axis carriage (cyan circles).
    - **Important**: please pay attention to the orientation of the parts relative to others. The pictures were taken at a different time, so some parts

![06_X_carriage_belt_locks_mounting_holes.png](./images/06_X_carriage_belt_locks_mounting_holes.png)

Expected result:

![06_X_carriage_belt_locks_final.png](./images/06_X_carriage_belt_locks_final.png)

## Mount the Z axis carriages {pagestep}

>! **Note**:
>! The pictures are outdated, but mostly equivalent relative to the procedure.

1. Gather:
    - The prepared Z axis carriages,
    - two [40cm_8mm_steel_rod](8mm_steel_rod){Qty: 2}, 
    - one [20cm_8mm_lead_screw](8mm_steel_rod){Qty: 1}, 
    - one [5x8_flexible_coupling](5x8_flexible_coupling){Qty:1},
    - one [608zz_bearing](608zz_bearing){Qty:1}.
2. Partially insert the rods into the holes on the Z axis base (red arrows).
3. Slide the rods into the Z axis carriage (orange square).
4. Insert the rods fully into the holes on the stepper side of the Z axis base (magenta arrows).
5. Fasten the 4 set screws gently (cyan arrows).

![06_Z_axis_carriage_01.png](./images/06_Z_axis_carriage_01.png)

6. Place the flexible coupling on the Z stepper shaft, and fasten it.

![06_Z_axis_carriage_02.png](./images/06_Z_axis_carriage_02.png)

7. Insert the lead screw through the bearing, screw it through the brass nut in the Z axis carriage, insert it into the flexible coupling, and fasten it.

![06_Z_axis_carriage_03.png](./images/06_Z_axis_carriage_03.png)

8. Finally slide the "top" Z axis carriage into the free ends of the 8mm rods.
    - Note: your slider should have a center hole to prevent a collision with the free end of the lead screw. Alternatively, cut your lead screw to a shorter length.

![06_Z_axis_carriage_04.png](./images/06_Z_axis_carriage_04.png)

## Congrats!

You have assembled the [Z axis parts]{output, Qty: 1}.

Return to the main step and continue: [01_cnc_frame.md](../01_cnc_frame.md)

Expected result:

![06_Z_axis_final_1_v2.jpg](./images/06_Z_axis_final_1_v2.jpg)


