# Z axis 3D printed parts

{{BOM}}

>i **Note**:
>i Under construction!

Print the core parts of the Z axis (which includes the X axis carriage).

![z-axis-toolchanger.png](./images/z-axis-toolchanger.png)

## Print the parts{pagestep}

Instructions:

1. Use a [3D printer](tools.yaml#3dprinter){Cat:tool,Qty:1} to print the parts for the Z axis.

STL files for the parts:

* [02_models_z_01.stl](./models/02_models_z_01.stl): use buildplate supports for this model.
    ![](./models/02_models_z_01.stl){color: deeppink}
* [02_models_z_02.stl](./models/02_models_z_02.stl): supports are optional for this model, its up to your experience.
        ![](./models/02_models_z_02.stl){color: deeppink}

Notes:

* The print used 10 [printer hours](tools.yaml#print_time){Qty:12.0}, and [PLA plastic filament](frameparts.yaml#PLA){Qty: 120 g}.
* The layer height was 0.3 mm.
* No supports were used.

![images/Z_slicer.png](images/Z_slicer.png)

## Congrats!

You have the 3D printed the [Z axis printed parts]{output, Qty: 1}.

Return to the main step and continue: [01_cnc_frame.md](../01_cnc_frame.md)