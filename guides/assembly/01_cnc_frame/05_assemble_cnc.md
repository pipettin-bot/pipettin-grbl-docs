# XYZ assembly

Assembly of the XYZ carriages into one connected system.

{{BOM}}

>i **Note**:
>i Under construction!

## Gather the prepared parts{pagestep}

From previous steps you will need:

* [XY axis parts](fromstep){Qty:1}
* [Z axis parts](fromstep){Qty:1}

## Assemble the XY frame{pagestep}

>! **Note**:
>! The pictures of the Z axis are outdated, but mostly equivalent relative to the procedure.

From previous steps you will need the prepared [XY axis parts](fromstep):

* 4 prepared Y axis bases (red squares),
* 2 prepared Y axis carriages; one with a stepper, the other with the tension pulley (blue squares),
* and the X axis + Z axis part (yellow square).

![06_XYZ_frame_parts.png](./images/06_XYZ_frame_parts.png)

1. Gather four [8mm_steel_rod](rods.yaml#8mm_steel_rod){Qty: 4} and two [12mm_steel_rod](rods.yaml#12mm_steel_rod){Qty: 2}.
2. Insert two 8 mm rods into the Y axis bases that has the cable carrier base (the taller one), and tighten the set screws slightly.
    - **Important**: the part will break if overtightened. For reference, tighten the screws until the rods dont fall off under their own weight.
3. Slide in the Y axis carriage that holds the X axis stepper.
    - **Important**: the orientation must be correct, such that: (1) the connector is facing upward, and (2) that the shaft faces away from teh first Y axis base (cyan arrows).
4. Insert the free end of the two rods into a second X axis base.
    - **Important**: the orientation must be correct, see pictures below (red arrows).

![06_XYZ_frame_Ybase_1.png](./images/06_XYZ_frame_Ybase_1.png)
![06_XYZ_frame_Ybase_2.png](./images/06_XYZ_frame_Ybase_2.png)

5. Repeat these steps for the remaining pair of Y axis bases (with no cable carrier base) and the remaining Y axis carriage (with the X axis pulley idler).
    - Note the orientation of the parts (se picture below). It must be correct.
    - Remember to gently fasten the 8mm rods using the set screws on both Y axis bases.

![06_XYZ_frame_Yframe2.png](./images/06_XYZ_frame_Yframe2.png)

6. Now insert about 15 cm of one of the 12 mm rods into the top hole on one of the Y axis carriages (starting from the "outer" side, red arrow).

![06_XYZ_frame_Yframe3_1.png](./images/06_XYZ_frame_Yframe3_1.png)

7. Slide the 12 mm rod into the bearings of the top part of the X axis carriage (dashed orange lines).

![06_XYZ_frame_Yframe3_4.png](./images/06_XYZ_frame_Yframe3_4.png)

8. After the 12 mm passes through the second bearing, place the remaining cable carrier base (green circle).
    -  Note: orientation is important, the rounded edge of the base must face the X axis carriage (green dashed lines and arrows).

![06_XYZ_frame_Yframe3_6.png](./images/06_XYZ_frame_Yframe3_6.png)
![06_XYZ_frame_Yframe3_6_2.png](./images/06_XYZ_frame_Yframe3_6_2.png)

9. Gently adjust the set screws on the Y axis carriages, to fix the 12 mm rod in place.

![06_XYZ_frame_Yframe3_7.png](./images/06_XYZ_frame_Yframe3_7.png)

10. Insert the remaining rod through the remaining holes in the Y axis carriages and the X axis carriage bearings (cyan arrows).

![06_XYZ_frame_Yframe3_8.png](./images/06_XYZ_frame_Yframe3_8.png)

11. Gently adjust all remaining set screws, to fix the 12 mm rods in place (magenta arrow).

![06_XYZ_frame_Yframe3_9.png](./images/06_XYZ_frame_Yframe3_9.png)


## Congrats!

You have assembled the [XYZ CNC frame]{output, Qty: 1}.

Return to the main step and continue: [01_cnc_frame.md](../01_cnc_frame.md)

>! **Note**:
>! The models of the Z axis in this picture are outdated.

Expected result:

![06_XYZ_axis_final.png](./images/06_XYZ_axis_final.png)
