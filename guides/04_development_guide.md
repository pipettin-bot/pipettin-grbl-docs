# Study & Modification Guide

>i **Note**: 
>i Under construction. See issue [https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/53](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/53).

This guide will cover:

* Design choices and rationales.
* Customizing software innards and hardware.
* Adding a new "X": platforms, workspaces, tools, …

For the time being, lots of development documentation can be found in the README files of the main repository, at [https://gitlab.com/pipettin-bot/pipettin-grbl/](https://gitlab.com/pipettin-bot/pipettin-grbl/).
