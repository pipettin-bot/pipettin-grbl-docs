# Contributions Guide

All kinds of contributions are very welcome!

For now we suggest:

* Joining the GOSH forum and posting here: [https://forum.openhardware.science/c/projects/54](https://forum.openhardware.science/c/projects/54)
* Using issues on GitLab:
    * About the hardware/software/firmware [here](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues).
    * About the documentation [here](https://gitlab.com/pipettin-bot/pipettin-grbl-docs/-/issues).

>i **Note**: 
>i Under construction. See issue [https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/51](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/51).

This guide will eventually cover:

* Project roadmap.
* Communication channels.
* More specific info on ways to contribute.
