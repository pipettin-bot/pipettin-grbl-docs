# Set-up & Maintenance Guide

>i **Note**: 
>i Under construction. See issue [https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/50](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/50).

Will cover:

* A global re-callibration procedure.
* Troubleshooting known problems.
* Repairing common failures.

## Post-assembly setup {pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

>i **Note**:
>i This section takes a couple hours.

If you have not done so already, follow the setup guide: [01_setup.md](./maintenance/01_setup.md){step}

## Post-assembly calibrations{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

Follow the calibration guide: [02_calibration.md](./maintenance/02_calibration.md){step}

## Maintenance

TO-DO

### Belt tension

TO-DO

### Lubrication

TO-DO

# Congrats!

You've set-up and taken care of your machine.

Come back to these pages once in a while, specially if you start encountering any issues.

You may move on to the [user guide](./01_user_guide.md), or go back to the [index](../index.md) page.

![maintenance/images/engineering_flowchart.png](maintenance/images/engineering_flowchart.png)
> As seen at [R-bloggers](https://www.r-bloggers.com/2022/11/learning-data-science-predictive-maintenance-with-decision-trees/).
