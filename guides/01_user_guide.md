# User Guide

>i **Note**: 
>i Under construction. See issue [https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/52](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/52).

Will cover:

* Software usage.
* Workspace setup.
* ...?

Link back to the [index.md](../index.md).

## Requirements

If you have not yet done so, please follow the setup and callibration guides thoroughly:

1. Setup guide: [01_setup.md](./maintenance/01_setup.md)
2. Callibration guide: [02_calibration.md](./maintenance/02_calibration.md)

## Place physical objects{pagestep}

The basic idea is to place objects along the 3D-printed _curbs_. Use these to align the objects easily, precisely and repeatably, to the workspace's surface.

1. Place the baseplate on the aluminium frame.
    - Make sure that the contact points are correctly aligned with the slots one the aluminium profile. It must sit in place firmly.
2. Place some _curbs_ on the baseplate.
    - Try to form corners at different places. This shape will be useful to align objects in the next step.
3. Place and align your objects on the baseplate, for example:
    - One p200 tip rack with tips.
    - One tube rack with some tubes.
    - A "discard tray" should be placed just below the tip-ejection post.

The relative placement of physical objects on the baseplate must correspond with the position of virtual objects in the GUI's workspace. This will be setup on the next step.

![modulitos2.png](./usage/images/modulitos2.png)

> Curbs on the baseplate.

![corners.jpg](./assembly/05_baseplate/images/corners.jpg)

> At first the objects are free to move. The curbs (the white 3D-printed part in "L" shape) should be square with the machines coordinates, and will help align the objects, in both position and angle.

![overview.jpg](./assembly/05_baseplate/images/overview.jpg)

> Once in place, the position of the objects must be calibrated in the GUI, such that the virtual representation is perfectly aligned with the physical objects.

## Find your Pi's local IP address{pagestep}

If you do not know your Pi's IP, follow the relevant section of the [setup guide](./maintenance/01_setup.md).

## Create a virtual workspace{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

A video example is available, demonstrating how to operate the GUI to setup a workspace, its contents, and a protocol.

- Watch the videos here: [photos.app.goo.gl/L4CYRKD2wZrmArp87](https://photos.app.goo.gl/L4CYRKD2wZrmArp87)

>i **Note**:
>i The videos are not meant to be self-explanatory. Specially the last one; I recorded it as a placeholder until I make a decent usage guide.

Steps:

1. Open the GUI by pointing your browser to the Pi's IP address on your local network, on port 3333.
    - For example: `http://192.168.0.12:3333`
2. Click on the "Workspace" drop-down menu.
3. Select "New" to create a workspace.
4. Click on the "Save" icon (the diskette) to save the new workspace and give it a name.
5. A "Workspace saved" message should appear.

![gui-01.png](./usage/images/gui-01.png)
![gui-02-workspacemenu.png](./usage/images/gui-02-workspacemenu.png)
![gui-03-blankworkspace.png](./usage/images/gui-03-blankworkspace.png)
![gui-04-savetheworkspace.png](./usage/images/gui-04-savetheworkspace.png)
![gui-05-workspacesavd.png](./usage/images/gui-05-workspacesavd.png)

TO-DO:

- Add annotations to the screenshots, naming the visible elements of the GUI.

## Add platforms to the workspace{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

06. addplatformmenu
07. addtipbox
08. dragtipbox
09. addmoreplatforms
10. selectitems
11. itemsselected
12. additemstotiprackmenu
13. tipsadded
14. itemselectedclick
15. addtubemenu
16. addtuberesult
17. moretubesselected
18. moretubesadded

![gui-06-addplatformmenu.png](./usage/images/gui-06-addplatformmenu.png)
![gui-07-addtipbox.png](./usage/images/gui-07-addtipbox.png)
![gui-08-dragtipbox.png](./usage/images/gui-08-dragtipbox.png)
![gui-09-addmoreplatforms.png](./usage/images/gui-09-addmoreplatforms.png)
![gui-10-selectitems.png](./usage/images/gui-10-selectitems.png)
![gui-11-itemsselected.png](./usage/images/gui-11-itemsselected.png)
![gui-12-additemstotiprackmenu.png](./usage/images/gui-12-additemstotiprackmenu.png)
![gui-13-tipsadded.png](./usage/images/gui-13-tipsadded.png)
![gui-14-itemselectedclick.png](./usage/images/gui-14-itemselectedclick.png)
![gui-15-addtubemenu.png](./usage/images/gui-15-addtubemenu.png)
![gui-16-addtuberesult.png](./usage/images/gui-16-addtuberesult.png)
![gui-17-moretubesselected.png](./usage/images/gui-17-moretubesselected.png)
![gui-18-moretubesadded.png](./usage/images/gui-18-moretubesadded.png)

## Align and calibrate platforms{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

The robot must be told _where_ each object is located.

Follow the relevant section on the [calibration guide](./maintenance/02_calibration.md).

## Create a protocol{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

## Pre-run considerations{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

* Final checks.
* What if something goes wrong?

## Run the protocol{pagestep}

>i **Note**:
>i This section is missing a proper tutorial.

# Congrats!

You've automated your lab life. Isn't that nice?

Move on to the [contributions guide](./05_contributions_guide.md).

What else could you do with so much free time, now back on your hands?

[![viñeta](https://imgs.xkcd.com/comics/automation.png)](https://xkcd.com/1319/)

# List of to-do's

Due improvements for this page:

- [ ] b286: _the pictures are small and I could find an option to zoom them_.